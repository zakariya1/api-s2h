﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Helper
{
    public class EnumHelper
    {

        public static class CodeTypeUse
        {
            public const string PROVIDER = "PVD";
            public const string CUSTOMER = "CTM";
            public const string SERVICECARD = "SVC";
            public const string BUCKET = "BU";
            public const string BOOKING = "BK";
        }
        public static class CodeTypeName
        {
            public const string PROVIDER = "PVD";
            public const string CUSTOMER = "CTM";
            public const string SERVICECARD = "SVC";
            public const string BUCKET = "BU";
            public const string BOOKING = "BK";

        }
        public enum RegisType
        {
            Normal = 1,
            Google = 2,
            Facebook = 3,
            Apple = 4,
            Line = 5

        }
        public enum EntityType
        {
            SERVICECARD  = 1,
            PROFILE_CUSTOMER = 2,
            PROFILE_PROVIDER = 3,
            Certification = 4,
            Booking_Review = 5,
            House_Registration = 6,
            PDF = 7,
            ORTHOR = 10
        }

        public enum UserType 
        {
            Customer = 1,
            Provider = 2,
        }
        public enum StatusBooking
        {
            Cancel = -2,
            Reject = -1,
            CreateRequest = 1,
            Accepted = 2,
            Pending = 3,
            In_Process = 4,
            Completed = 5,
            Closed = 6
        }
        public static class ActivityBooking
        {
            //public const string Cancel = "Cancel";
            //public const string Reject = "Reject";
            //public const string CreateRequest = "Create Request";
            //public const string Paid = "Paid";
            //public const string Accept = "Accepted by Provider";
            //public const string Closed = "Closed";

            //public const string Cancel = "ยกเลิก";
            //public const string Reject = "ยกเลิก";
            //public const string CreateRequest = "รอชำระเงิน";
            //public const string Paid = "อยู่ระหว่างดำเนินการ";
            //public const string Accept = "ยืนยัน";
            //public const string Closed = "เสร็จสิ้น";

            public const string Cancel = "ยกเลิก";
            public const string Reject = "ยกเลิก";
            public const string CreateRequest = "รอยืนยันการจอง";
            public const string Waiting_PVD_Confirm = "รอชำระเงิน";
            public const string Pending = "รอดำเนินการ";
            public const string In_Process = "อยู่ระหว่างดำเนินการ";
            public const string Completed = "ดำเนินการเสร็จสิ้น";
            public const string Closed = "เสร็จสิ้นการให้บริการ";

        }

        public static void DeleteFile(string file)
        {
            if (File.Exists(file))
            {
                File.Delete(file);
            }
        }
    }
}
