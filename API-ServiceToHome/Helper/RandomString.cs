﻿using API_ServiceToHome.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Helper
{
    public class RandomString
    {
        readonly DBContext _context;
        public RandomString(DBContext context)
        {
            _context = context;
        }
        private static Random random = new Random();
        private static string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefjfijklmnopqrstuvwxyz";
        public string Generate()
        {
            // var result = null;//_context.Provider_register.Where(o => o.Provider_ID == TextRandom(6))

            return null;
        }

        public static string TextRandom(int length)
        {
            return new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
