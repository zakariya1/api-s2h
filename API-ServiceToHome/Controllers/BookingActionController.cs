﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_ServiceToHome.Data;
using API_ServiceToHome.Models;
using API_ServiceToHome.Models.InputModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API_ServiceToHome.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class BookingActionController : ControllerBase
    {
        private string EventStatus { get; set; } = "";
        private readonly IBookingAction bookingAction;
        private ITransactionLogService transactionLogService;
        
        public BookingActionController(IBookingAction bookingAction, ITransactionLogService transactionLogService)
        {
            this.bookingAction = bookingAction;
            this.transactionLogService = transactionLogService;
        }



        // POST: api/BookingAction
        [HttpPost("CreateBooking", Name = "CreateBooking")]
        public IActionResult CreateBooking(BookingInputModel objData)
        {
            var objResult = (dynamic)null;
            if (objData == null)
            {
                return NotFound();
            }
            try
            {
                objResult = bookingAction.CreateRequest(objData);
                return Ok(objResult);
            }
            catch (Exception ex)
            {
                EventStatus = ex.Message.ToString();
                return BadRequest(ex.Message);
            }
            finally
            {
                TransactionLog log = new TransactionLog();
                log.ActivityType = "Booking";
                log.EntityName = "CreateBooking";
                log.EntityTypeID = null;
                log.ReferID = null;
                log.RequestData = Newtonsoft.Json.JsonConvert.SerializeObject(objData);
                log.ResultDetail = Newtonsoft.Json.JsonConvert.SerializeObject(objResult);
                log.Detail = EventStatus == "" ? null : EventStatus;
                log.Result = EventStatus == "" ? "success" : "error";
                transactionLogService.LogInsert(log);
            }

        }


        [HttpPost("AssignBookingToProvider", Name = "AssignBookingToProvider")]
        public IActionResult AssignBookingToProviderJob(BookingAssignJob objData = null)
        {
            var objResult = (dynamic)null;
            if (objData == null)
            {
                return NotFound();
            }
            try
            {
                objResult = objData;
                bookingAction.AssignBookingToProvider(objData);
                return Ok();
            }
            catch (Exception ex)
            {
                EventStatus = ex.Message;
                return BadRequest(ex.Message);
            }
            finally
            {
                TransactionLog log = new TransactionLog();
                log.ActivityType = "Booking";
                log.EntityName = "AssignBookingToProvider";
                log.EntityTypeID = null;
                log.ReferID = null;
                log.RequestData = Newtonsoft.Json.JsonConvert.SerializeObject(objData);
                log.ResultDetail = null;
                log.Detail = EventStatus == "" ? null : EventStatus;
                log.Result = EventStatus == "" ? "success" : "error";
                transactionLogService.LogInsert(log);
            }
        }


        [HttpPost("AcceptBooking/{bookId}", Name = "AcceptBooking")]
        public IActionResult AcceptBooking(int? bookId = 0)
        {
            var objResult = (dynamic)null;
            if (bookId == 0)
            {
                return NotFound();
            }
            try
            {
                bookingAction.Accepted(bookId);
                
                return Ok();
            }
            catch (Exception ex)
            {
                EventStatus = ex.Message;
                return BadRequest(ex.Message);
            }
            finally
            {
                TransactionLog log = new TransactionLog();
                log.ActivityType = "Booking";
                log.EntityName = "AcceptBooking";
                log.EntityTypeID = null;
                log.ReferID = bookId.ToString(); 
                log.RequestData = bookId.ToString();
                log.ResultDetail = null;
                log.Detail = EventStatus == "" ? null : EventStatus;
                log.Result = EventStatus == "" ? "success" : "error";
                transactionLogService.LogInsert(log);
            }
        }
        [HttpPost("PendingBooking", Name = "PendingBooking")]
        public IActionResult PendingBooking(BookIDOrBookCode objData =  null)
        {
            var objResult = (dynamic)null;
            if (objData == null)
            {
                return NotFound();
            }
            try
            {
                objResult = objData;
                bookingAction.Pending(objData);
                return Ok();
            }
            catch (Exception ex)
            {
                EventStatus = ex.Message;
                return BadRequest(ex.Message);
            }
            finally
            {
                TransactionLog log = new TransactionLog();
                log.ActivityType = "Booking";
                log.EntityName = "PendingBooking";
                log.EntityTypeID = null;
                log.ReferID = null;
                log.RequestData = Newtonsoft.Json.JsonConvert.SerializeObject(objData);
                log.ResultDetail = null;
                log.Detail = EventStatus == "" ? null : EventStatus;
                log.Result = EventStatus == "" ? "success" : "error";
                transactionLogService.LogInsert(log);
            }
        }
        [HttpPost("InProcessBooking/{bookId}", Name = "InProcessBooking")]
        public IActionResult InProcessBooking(int? bookId = 0)
        {
            var objResult = (dynamic)null;
            if (bookId == 0)
            {
                return NotFound();
            }
            try
            {
                bookingAction.InProcess(bookId);
                return Ok();
            }
            catch (Exception ex)
            {
                EventStatus = ex.Message;
                return BadRequest(ex.Message);
            }
            finally
            {
                TransactionLog log = new TransactionLog();
                log.ActivityType = "Booking";
                log.EntityName = "InProcessBooking";
                log.EntityTypeID = null;
                log.ReferID = bookId.ToString();
                log.RequestData = bookId.ToString();
                log.ResultDetail = null;
                log.Detail = EventStatus == "" ? null : EventStatus;
                log.Result = EventStatus == "" ? "success" : "error";
                transactionLogService.LogInsert(log);
            }
        }
        [HttpPost("CompleteBooking", Name = "CompleteBooking")]
        public IActionResult CompleteBooking(BookingActionInputModel objData)
        {
            if (objData == null)
            {
                return NotFound();
            }
            try
            {
                bookingAction.Completed(objData);
                return Ok();
            }
            catch (Exception ex)
            {
                EventStatus = ex.Message;
                return BadRequest(ex.Message);
            }
            finally
            {
                TransactionLog log = new TransactionLog();
                log.ActivityType = "Booking";
                log.EntityName = "CompleteBooking";
                log.EntityTypeID = null;
                log.ReferID = objData.BookID.ToString();
                log.RequestData = Newtonsoft.Json.JsonConvert.SerializeObject(objData);
                log.ResultDetail = null;
                log.Detail = EventStatus == "" ? null : EventStatus;
                log.Result = EventStatus == "" ? "success" : "error";
                transactionLogService.LogInsert(log);
            }
        }
        [HttpPost("CloseBooking", Name = "CloseBooking")]
        public IActionResult CloseBooking(BookingActionInputModel objData)
        {
            if (objData == null)
            {
                return NotFound();
            }
            try
            {
                bookingAction.Closed(objData);
                return Ok();
            }
            catch (Exception ex)
            {
                EventStatus = ex.Message;
                return BadRequest(ex.Message);
            }
            finally
            {
                TransactionLog log = new TransactionLog();
                log.ActivityType = "Booking";
                log.EntityName = "CloseBooking";
                log.EntityTypeID = null;
                log.ReferID = objData.BookID.ToString();
                log.RequestData = Newtonsoft.Json.JsonConvert.SerializeObject(objData);
                log.ResultDetail = null;
                log.Detail = EventStatus == "" ? null : EventStatus;
                log.Result = EventStatus == "" ? "success" : "error";
                transactionLogService.LogInsert(log);
            }
        }
        [HttpPost("CancelBooking", Name = "CancelBooking")]
        public IActionResult CancelBooking(BookingActionInputModel objData)
        {
            var objResult = (dynamic)null;
            if (objData == null)
            {
                return NotFound();
            }
            try
            {
                bookingAction.Canceled(objData);
                return Ok();
            }
            catch (Exception ex)
            {
                EventStatus = ex.Message;
                return BadRequest(ex.Message);
            }
            finally
            {
                TransactionLog log = new TransactionLog();
                log.ActivityType = "Booking";
                log.EntityName = "CancelBooking";
                log.EntityTypeID = null;
                log.ReferID = objData.BookID.ToString();
                log.RequestData = Newtonsoft.Json.JsonConvert.SerializeObject(objData);
                log.ResultDetail = null;
                log.Detail = EventStatus == "" ? null : EventStatus;
                log.Result = EventStatus == "" ? "success" : "error";
                transactionLogService.LogInsert(log);
            }
        }
        [HttpPost("RejectBooking", Name = "RejectBooking")]
        public IActionResult RejectBooking(BookingActionInputModel objData)
        {
            if (objData == null)
            {
                return NotFound();
            }
            try
            {
                bookingAction.Reject(objData);
                return Ok();
            }
            catch (Exception ex)
            {
                EventStatus = ex.Message;
                return BadRequest(ex.Message);
            }
            finally
            {
                TransactionLog log = new TransactionLog();
                log.ActivityType = "Booking";
                log.EntityName = "RejectBooking";
                log.EntityTypeID = null;
                log.ReferID = objData.BookID.ToString();
                log.RequestData = Newtonsoft.Json.JsonConvert.SerializeObject(objData);
                log.ResultDetail = null;
                log.Detail = EventStatus == "" ? null : EventStatus;
                log.Result = EventStatus == "" ? "success" : "error";
                transactionLogService.LogInsert(log);
            }
        }

    }
}
