﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_ServiceToHome.Data;
using API_ServiceToHome.Models;
using API_ServiceToHome.Models.InputModels;
using API_ServiceToHome.Models.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API_ServiceToHome.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class BookingController : ControllerBase
    {
        private readonly IBooking booking;


        public BookingController(IBooking booking)
        {
            this.booking = booking;
        }

        // GET: api/Booking
        [HttpGet]
        public IEnumerable<Booking> GetBooking()
        {
            return null;
        }

        // GET: api/Booking/5
        [HttpGet("{id}", Name = "GetBooking")]
        public Booking GetBooking(int id)
        {
            return null;
        }

        // POST: api/Booking
        [HttpPost]
        public IActionResult Post([FromBody] BookingInputModel input)
        {

            try
            {
                booking.CreateBooking(input);
                booking.Save();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("GetBookingFillter", Name = "GetBookingFillter")]
        public IEnumerable<BookingModel> GetBookingFillter(BookingFillter fillter)
        {
            return booking.GetAllBookingFillter(fillter);
        }


        [HttpPost("GetBookingInProcess", Name = "GetBookingInProcess")]
        public async Task<ActionResult<IEnumerable<BookingModel>>> GetBookingInProcess(int CustomerId)
        {
            if (CustomerId == null || CustomerId == 0) { return NotFound();  }

            try
            {
                BookingFillter filter = new BookingFillter();
                filter.CustomerID = CustomerId;
                filter.BookingStatusID = new List<int?>() { 1,2,3,4,5 };
                return Ok(booking.GetAllBookingFillter(filter));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpPost("GetBookingProviderInProcess", Name = "GetBookingProviderInProcess")]
        public async Task<ActionResult<IEnumerable<BookingModel>>> GetBookingProviderInProcess(int Provider)
        {
            if (Provider == null || Provider == 0) { return NotFound(); }

            try
            {
                BookingFillter filter = new BookingFillter();
                filter.ProviderID = Provider;
                filter.BookingStatusID = new List<int?>() {  1,2, 3, 4, 5 };
                return Ok(booking.GetAllBookingProviderFillter(filter));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("GetBookingProviderHistory", Name = "GetBookingProviderHistory")]
        public async Task<ActionResult<IEnumerable<BookingModel>>> GetBookingProviderHistory(int ProviderID)
        {
            if (ProviderID == null || ProviderID == 0) { return NotFound(); }

            try
            {
                BookingFillter filter = new BookingFillter();
                filter.ProviderID = ProviderID;
                filter.BookingStatusID = new List<int?>() { -1, -2, 6 };
                return Ok(booking.GetAllBookingProviderFillter(filter));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("GetBookingHistory", Name = "GetBookingHistory")]
        public async Task<ActionResult<IEnumerable<BookingModel>>> GetBookingHistory(int CustomerId)
        {
            if (CustomerId == null || CustomerId == 0) { return NotFound(); }

            try
            {
                BookingFillter filter = new BookingFillter();
                filter.CustomerID = CustomerId;
                filter.BookingStatusID = new List<int?>() { -1, -2 ,6 };
                return Ok(booking.GetAllBookingFillter(filter));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet("GetBookingByBookID/{bookid}", Name = "GetBookingByBookID")]
        public BookingModel GetBookingByBookID(int bookid)
        {
            return booking.GetBookingByBookID(bookid);
        }
    }
}
