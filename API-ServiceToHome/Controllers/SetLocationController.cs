﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_ServiceToHome.Data;
using API_ServiceToHome.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API_ServiceToHome.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class SetLocationController : ControllerBase
    {
        private string EventStatus { get; set; } = "";
        private readonly DBContext _context;
        private ITransactionLogService transactionLogService;

        public SetLocationController(DBContext context, ITransactionLogService transactionLogService)
        {
            _context = context;
            this.transactionLogService = transactionLogService;
        }



        // POST: api/SetLocation
        [HttpPost]
        public IActionResult Post([FromBody] SetLocation value)
        {
            //var amount = 1000;
            //double  persen = 3.5;
            //double output = amount - ((amount / 100) * persen);
            //var objData = value;
            try
            {
                if (value.Type == SetLocation.EntityType.CUSTOMER)
                {
                    var result = _context.CustomerRegister.FirstOrDefault(o => o.CustomerID == value.ReferID);
                    if (result != null)
                    {
                        result.Lat = value.Lat;
                        result.Long = value.Long;
                        _context.CustomerRegister.Update(result);
                        _context.SaveChanges();
                        return Ok(new { status = true, message = "Update location successfully" });
                    }
                }
                else if (value.Type == SetLocation.EntityType.PROVIDER)
                {
                    var result = _context.ProviderRegister.FirstOrDefault(o => o.ProviderID == value.ReferID);
                    if (result != null)
                    {
                        result.Lat = value.Lat;
                        result.Long = value.Long;
                        _context.ProviderRegister.Update(result);
                        _context.SaveChanges();
                        return Ok(new { status = true, message = "Update location successfully" });
                    }
                }
                else if (value.Type == SetLocation.EntityType.BOOKING)
                {
                    var result = _context.Booking.FirstOrDefault(o => o.BookID == value.ReferID);
                    if (result != null)
                    {
                        result.Lat = value.Lat;
                        result.Long = value.Long;
                        _context.Booking.Update(result);
                        _context.SaveChanges();
                        return Ok(new { status = true, message = "Update location successfully" });
                    }
                }

                return Ok(new { status = true, message = "Update location successfully" });
            }
            catch (Exception ex)
            {
                EventStatus = ex.Message;
                return BadRequest(ex.Message);
            }
            finally
            {
                TransactionLog log = new TransactionLog();
                log.ActivityType = "Location";
                log.EntityName = "SetLocation";
                log.EntityTypeID = null;
                log.ReferID = null;
                log.RequestData = Newtonsoft.Json.JsonConvert.SerializeObject(value);
                log.ResultDetail = null;
                log.Detail = EventStatus;
                log.Result = EventStatus == "" ? "success" : "error";
                transactionLogService.LogInsert(log);
            }
        }

    }
}
