﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_ServiceToHome.Data;
using API_ServiceToHome.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API_ServiceToHome.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class WalletProviderController : ControllerBase
    {
        readonly DBContext _context;
        public WalletProviderController(DBContext context)
        {
            _context = context;
        }
        // GET: api/WalletProvider
        [HttpGet]
        public IEnumerable<WalletProvider> GetWalletProvider()
        {
            return _context.WalletProvider.ToList();
        }

        // GET: api/WalletProvider/5
        [HttpGet("{id}", Name = "GetWalletProvider")]
        public WalletProvider GetWalletProvider(int id)
        {
            return _context.WalletProvider.FirstOrDefault(o => o.WalletProviderID == id);
        }

        // POST: api/WalletProvider
        [HttpPost]
        public void Post([FromBody] WalletProvider value)
        {
            _context.WalletProvider.Add(value);
            _context.SaveChanges();
        }

    }
}
