﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_ServiceToHome.Data;
using API_ServiceToHome.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API_ServiceToHome.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class TypeRegisterController : ControllerBase
    {
        readonly DBContext _context;
        public TypeRegisterController(DBContext context)
        {
            _context = context;
        }
        // GET: api/TypeRegister
        [HttpGet]
        public IEnumerable<TypeRegister> GetTypeRegister()
        {
            return _context.TypeRegister.ToList();
        }

        // GET: api/TypeRegister/5
        [HttpGet("{id}", Name = "GetTypeRegister")]
        public TypeRegister GetTypeRegister(int id)
        {
            return _context.TypeRegister.FirstOrDefault(o => o.TypeRegisterID == id);
        }

        // POST: api/TypeRegister
        [HttpPost]
        public void Post([FromBody] TypeRegister value)
        {
            _context.TypeRegister.Add(value);
            _context.SaveChanges();
        }

    }
}
