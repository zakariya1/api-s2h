﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_ServiceToHome.Data;
using API_ServiceToHome.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API_ServiceToHome.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionLogController : ControllerBase
    {
        private ITransactionLogService transactionLogService;

        public TransactionLogController(ITransactionLogService transactionLogService)
        {
            this.transactionLogService = transactionLogService;
        }

        // POST: api/TransactionLog
        [HttpPost]
        public void Post([FromBody] TransactionLog model)
        {
            transactionLogService.LogInsert(model);
        }
    }
}
