﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_ServiceToHome.Data;
using API_ServiceToHome.Models;
using API_ServiceToHome.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API_ServiceToHome.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class CustomerBasketController : ControllerBase
    {
        private readonly IBasket basket;
        readonly DBContext _context;

        public CustomerBasketController(IBasket basket, DBContext context)
        {
            this.basket = basket;
            _context = context;
        }

        // GET: api/CustomerBasket
        [HttpGet]
        public IEnumerable<CustomerBasket> GetCustomerBasket()
        {
            return _context.CustomerBasket.ToList();
        }

        // GET: api/CustomerBasket/5
        [HttpGet("{id}", Name = "GetCustomerBasket")]
        public CustomerBasket GetCustomerBasket(int id)
        {
            return _context.CustomerBasket.FirstOrDefault(o => o.CustomerBasketID == id);
        }

        // POST: api/CustomerBasket
        [HttpPost]
        public CustomerBasket Post([FromBody] CustomerBasket objData)
        {
            return basket.CreateBasket(objData);
            //var result = _context.CustomerBasket.Where(o => o.CustomerBasketID == value.CustomerBasketID).FirstOrDefault();
            //if (result == null)
            //{
            //    GenerateCode gen = new GenerateCode(_context);
            //    value.BasketCode = gen.CreateCode(Helper.EnumHelper.CodeTypeName.BUCKET);
            //    _context.CustomerBasket.Add(value);
            //    _context.SaveChanges();
            //}
        }
    }
}
