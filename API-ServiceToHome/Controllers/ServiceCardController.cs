﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_ServiceToHome.Data;
using API_ServiceToHome.Models;
using API_ServiceToHome.Models.InputModels;
using API_ServiceToHome.Models.View;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace API_ServiceToHome.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class ServiceCardController : ControllerBase
    {
        private readonly IServiceCard serviceCard;
        readonly DBContext _context;

        private ITransactionLogService transactionLogService;

        public ServiceCardController(IServiceCard serviceCard, DBContext context, ITransactionLogService transactionLogServicel)
        {
            this.serviceCard = serviceCard;
            _context = context;
            this.transactionLogService = transactionLogServicel;
        }

        private string EventStatus { get; set; } = "";

        // GET: api/ServiceCard
        [HttpGet("GetByProvid" +
            "erId{ProviderId}", Name = "GetByProviderId")]
        public List<ServiceCardModel> GetByProviderId(int ProviderId)
        {
            return serviceCard.GetServiceCardByProviderId(ProviderId);
        }
        [HttpGet("GetAllView", Name = "GetAllView")]
        public IEnumerable<ServiceCardModel> GetAllView()
        {
            return serviceCard.GetServiceCardAll();
        }
        [HttpPost("GetServiceCardFillter", Name = "GetServiceCareFIllter")]
        public IEnumerable<ServiceCardModel> GetServiceCareFIllter(ServiceCardFillter fillter)
        {
            return serviceCard.GetServiceCardFillter(fillter);
        }

        [HttpPost("SearchServiceCard", Name = "SearchServiceCard")]
        public IEnumerable<ServiceCardModel> SearchServiceCard(FilterSearchService Wording)
        {
            return serviceCard.SearchServiceCard(Wording);
        }
        // POST: api/ServiceCard
        [HttpPost("AddServiceCard")]
        public IActionResult Post([FromBody] ServiceCard value)
        {
            var objData = value;
            if (value == null)
            {
                return NotFound();
            }
            try
            {
                serviceCard.AddServiceCared(value);
                serviceCard.Save();
                return Ok();
            }
            catch (Exception ex)
            {
                EventStatus = ex.Message;
                return BadRequest(ex.Message);
            }
            finally
            {
                TransactionLog log = new TransactionLog();
                log.ActivityType = "ServiceCard";
                log.EntityName = "AddServiceCard";
                log.EntityTypeID = (int)Helper.EnumHelper.EntityType.SERVICECARD;
                log.ReferID = value.ServiceCardID.ToString() == "" ? "" : value.ServiceCardID.ToString();
                log.RequestData = Newtonsoft.Json.JsonConvert.SerializeObject(objData);
                log.ResultDetail = null;
                log.Detail = EventStatus;
                log.Result = EventStatus == "" ? "success" : "error";
                transactionLogService.LogInsert(log);
            }
        }
        [HttpPut("UpdateServiceCard/{ServiceCardId}")]
        public IActionResult Put(int ServiceCardId ,[FromBody] ServiceCard value)
        {
            if (ServiceCardId == 0)
            {
                return NotFound();
            }
            try
            {
                serviceCard.UpdateServiceCared(ServiceCardId, value);
                serviceCard.Save();
                return Ok();
            }
            catch (Exception ex)
            {
                EventStatus = ex.Message;
                return BadRequest(ex.Message);
            }
            finally
            {
                TransactionLog log = new TransactionLog();
                log.ActivityType = "ServiceCard";
                log.EntityName = "UpdateServiceCard";
                log.EntityTypeID = null;
                log.ReferID = value.ServiceCardID.ToString() == "" ? "" : value.ServiceCardID.ToString();
                log.RequestData = Newtonsoft.Json.JsonConvert.SerializeObject(value);
                log.ResultDetail = null;
                log.Detail = EventStatus;
                log.Result = EventStatus == "" ? "success" : "error";
                transactionLogService.LogInsert(log);
            }
        }
        [HttpDelete("/{ServiceCardId}")]
        public IActionResult Delete(int ServiceCardId = 0)
        {
            if (ServiceCardId == 0)
            {
                return NotFound();
            }
            try
            {
                serviceCard.RemoveServiceCared(ServiceCardId);
                serviceCard.Save();
                return Ok();
            }
            catch
            {
                return BadRequest();
            }
        }
    }
}
