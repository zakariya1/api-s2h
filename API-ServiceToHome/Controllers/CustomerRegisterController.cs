﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API_ServiceToHome.Data;
using API_ServiceToHome.Helper;
using API_ServiceToHome.Models;
using API_ServiceToHome.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace API_ServiceToHome.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class CustomerRegisterController : ControllerBase
    {
        private string EventStatus { get; set; } = "";
        private readonly DBContext _context;
        private readonly IEmailService email;
        private IFactoryService factoryService;
        private ITransactionLogService transactionLogService;
        private static string DirectoryPath  { get; set; } = "";

        public CustomerRegisterController(DBContext context, IEmailService email, IFactoryService factoryService, ITransactionLogService transactionLogService)
        {
            _context = context;
            this.email = email;
            this.factoryService = factoryService;
            this.transactionLogService = transactionLogService;
            DirectoryPath = factoryService.GetAppSetting("Common", "ViewFileUrl");
        }

        


        // GET: api/CustomerRegister


        [HttpGet]
        public IEnumerable<CustomerRegister> GetCustomerRegister()
        {
            List<CustomerRegister> customerlist = new List<CustomerRegister>();
            var customers = _context.CustomerRegister.ToList();
            foreach (var item in customers)
            {
                var img_url = _context.AttachmentFile.OrderByDescending(x => x.CreatedDate)
                    .FirstOrDefault(o => o.ReferID == item.CustomerID 
                    && o.EntityTypeID == (int)Helper.EnumHelper.EntityType.PROFILE_CUSTOMER);

                if (img_url != null && img_url.FilePath != null)
                {
                    item.ProfileImg = DirectoryPath + img_url.FilePath;
                }
                customerlist.Add(item);
            }

            return customerlist;
        }

        // GET: api/CustomerRegister/5
        [HttpGet("{id}", Name = "GetCustomerRegister")]
        public CustomerRegister GetCustomerRegister(int id)
        {
            var customer = _context.CustomerRegister.FirstOrDefault(o => o.CustomerID == id);
            
            if (customer != null)
            {
                var img_url = _context.AttachmentFile.OrderByDescending(x => x.CreatedDate)
                    .FirstOrDefault(o => o.ReferID == id && o.EntityTypeID == (int)Helper.EnumHelper.EntityType.PROFILE_CUSTOMER);

                if(img_url != null && img_url.FilePath != null)
                customer.ProfileImg = DirectoryPath + img_url.FilePath;
            }
            return customer;
        }

        // POST: api/CustomerRegister
        [HttpPost]
        public IActionResult Post([FromBody] CustomerRegister value)
        {
            CustomerRegister result = null; //overkill
            string EntityName = string.Empty;
            try
            {
                if (value == null)
                {
                    return NotFound();
                }

                int _profileCustomer = (int)Helper.EnumHelper.EntityType.PROFILE_CUSTOMER;
                
                if (value.RegisterType == (int)Helper.EnumHelper.RegisType.Normal)
                {
                    EntityName = Helper.EnumHelper.RegisType.Normal.ToString();
                    result = _context.CustomerRegister.Where(o => o.Email == value.Email
                    && o.RegisterType == (int)Helper.EnumHelper.RegisType.Normal).FirstOrDefault();
                    if (result != null)
                    {
                        var img_url = _context.AttachmentFile.OrderByDescending(x => x.CreatedDate)
                            .FirstOrDefault(o => o.ReferID == result.CustomerID
                            && o.EntityTypeID == _profileCustomer);

                        if (img_url != null && img_url.FilePath != null)
                            result.ProfileImg = DirectoryPath + img_url.FilePath;
                    }
                }
                else if (value.RegisterType == (int)Helper.EnumHelper.RegisType.Google)
                {
                    EntityName = Helper.EnumHelper.RegisType.Google.ToString();
                    result = _context.CustomerRegister.Where(o => o.Email == value.Email
                    && o.RegisterType == (int)Helper.EnumHelper.RegisType.Google).FirstOrDefault();
                    if (result != null)
                    {
                        var img_url = _context.AttachmentFile.OrderByDescending(x => x.CreatedDate)
                            .FirstOrDefault(o => o.ReferID == result.CustomerID
                            && o.EntityTypeID == _profileCustomer);

                        if (img_url != null && img_url.FilePath != null)
                            result.ProfileImg = DirectoryPath + img_url.FilePath;
                    }
                }
                else if (value.RegisterType == (int)Helper.EnumHelper.RegisType.Apple)
                {
                    EntityName = Helper.EnumHelper.RegisType.Apple.ToString();
                    result = _context.CustomerRegister.Where(o => o.RegisterType == 
                    (int)Helper.EnumHelper.RegisType.Apple
                    &&  o.TokenId.ToLower() == value.TokenId.ToLower()).FirstOrDefault();
                    if (result != null)
                    {
                        var img_url = _context.AttachmentFile.OrderByDescending(x => x.CreatedDate)
                            .FirstOrDefault(o => o.ReferID == result.CustomerID
                            && o.EntityTypeID == _profileCustomer);

                        if (img_url != null && img_url.FilePath != null)
                            result.ProfileImg = DirectoryPath + img_url.FilePath;
                    }

                    if (string.IsNullOrEmpty(value.TokenId))
                    {
                        if (result != null)
                        {
                            return Ok(new { msg = "data duplicate", status = true, data = result });
                        }
                        return Ok(new { msg = "sign in with Apple not working", status = true, data = "" });
                    }
                }
                else if (value.RegisterType == (int)Helper.EnumHelper.RegisType.Facebook)
                {
                    EntityName = Helper.EnumHelper.RegisType.Facebook.ToString();
                    result = _context.CustomerRegister
                        .Where(o => o.RegisterType == (int)Helper.EnumHelper.RegisType.Facebook
                        && o.TokenId.ToLower() == value.TokenId.ToLower()
                        ).FirstOrDefault();

                    if (result != null)
                    {
                        var img_url = _context.AttachmentFile.OrderByDescending(x => x.CreatedDate)
                            .FirstOrDefault(o => o.ReferID == result.CustomerID
                            && o.EntityTypeID == _profileCustomer);

                        if (img_url != null && img_url.FilePath != null)
                            result.ProfileImg = DirectoryPath + img_url.FilePath;
                    }
                }
                else
                {
                    return Ok(new
                    {
                        msg = "please key id register type!",
                        status = true,
                        data = ""
                    });
                }

                if (result == null)
                {
                    using (var transaction = _context.Database.BeginTransaction())
                    {
                        GenerateCode gen = new GenerateCode(_context);
                        value.isActive = true;
                        value.isVerify = value.RegisterType == (int)Helper.EnumHelper.RegisType.Normal ? false : true;
                        value.Username = value.Email;
                        value.Password = value.Password == null ? "" : SecurityHelper.GetMD5(value.Password);
                        value.CreatedBy = 0;
                        value.CreatedDate = DateTime.Now;
                        value.CustomerCode = gen.CreateCode(Helper.EnumHelper.CodeTypeName.CUSTOMER);
                        value.UserGUID = Guid.NewGuid().ToString();

                        _context.CustomerRegister.Add(value);
                        _context.SaveChanges();

                        result = value;

                        if (!string.IsNullOrEmpty(value.Email))
                        {
                            #region
                            //Email m = new Email();
                            //string UrlActivate = factoryService.GetAppSetting("Email", "UrlActiveCustomer") + value.UserGUID;

                            //StringBuilder sb = new StringBuilder();
                            //sb.AppendLine("<h1>Thank you for signing up with us.</h1> <br><br>");
                            //sb.AppendLine("Please <b>verify</b> your email address by clicking the link below: <br>");
                            //sb.AppendLine($"<h2><font color=#336699><b><a href = '{UrlActivate}'>Verify email address</a></b></font></h2> <br><br><br><br>");
                            //sb.AppendLine("if you have any questions, just contact us.<br>");
                            //sb.AppendLine("<img src = 'https://apisvth.wherey.co/api/UploadAndPreview/20200525ORTHOR_9669a647-810f-4ec4-9638-ae91d72bed20.png' style='width: 15px; height: 15px; '>");
                            //sb.AppendLine("<font color=#336699><b><a href = 'https://line.me/R/ti/p/%40716aeeml'>Line</a></b></font><br>");
                            //sb.AppendLine("<img src = 'https://apisvth.wherey.co/api/UploadAndPreview/20200525ORTHOR_504ac020-71ae-45eb-b924-0d31254547d3.png' style='width: 15px; height: 15px; '>");
                            //sb.AppendLine("<font color=#336699><b><a href = 'https://line.me/R/ti/p/%40716aeeml'>Facebook</a></b></font><br>");
                            //sb.AppendLine("<img src = 'https://apisvth.wherey.co/api/UploadAndPreview/20200525ORTHOR_2ef3e045-093b-4b24-885b-57868f31f81f.png' style='width: 15px; height: 15px; '>");
                            //sb.AppendLine("<font color=#336699><b><a href = 'https://line.me/R/ti/p/%40716aeeml'>Website</a></b></font><br><br>");
                            //sb.AppendLine("Best regards, <br>Serve Service Team.");

                            //m.Subject = "Please verify your email address.";
                            //m.Body = sb.ToString();
                            ////m.Body = $"please your click url {UrlActivate}. thak you.";
                            //m.To = value.Email;
                            #endregion
                            if (value.RegisterType == (int)Helper.EnumHelper.RegisType.Normal)
                            {
                                bool flag = email.SendEmailActive(value.UserGUID, value.Email, Helper.EnumHelper.CodeTypeUse.CUSTOMER);
                            }
                        }

                        transaction.Commit();
                    }

                    return Ok(new
                    {
                        msg = "regis success",
                        status = true,
                        data = result
                    });
                }
                else
                {
                    if (result.isActive == false && result.RegisterType == (int)Helper.EnumHelper.RegisType.Normal)
                    {
                        return Ok(new
                        {
                            msg = "active email",
                            status = true,
                            data = result
                        });
                    }
                    else
                    {
                        return Ok(new
                        {
                            msg = "data duplicate",
                            status = true,
                            data = result
                        });
                    }
                }

            }
            catch (Exception ex)
            {
                EventStatus = ex.Message;
                return BadRequest(ex.Message);
            }
            finally
            {
                #region
                TransactionLog log = new TransactionLog();
                log.ActivityType = "AuthenCustomer";
                log.EntityName = EntityName;
                log.EntityTypeID = null;
                log.ReferID = result.CustomerID.ToString();
                log.RequestData = Newtonsoft.Json.JsonConvert.SerializeObject(value);
                log.ResultDetail = Newtonsoft.Json.JsonConvert.SerializeObject(result);
                log.Detail = Newtonsoft.Json.JsonConvert.SerializeObject(EventStatus); ;
                log.Result = EventStatus == "" ? "success" : "error";
                transactionLogService.LogInsert(log);
                #endregion
            }

        }

        [HttpPost("UpdateCustomer", Name= "UpdateCustomer")]
        public IActionResult UpdateCustomer(CustomerRegister objData)
        {
            var objResult = (dynamic)null;
            if (objData == null)
            {
                return NotFound();
            }

            try
            {
                var r = _context.CustomerRegister
                                .FirstOrDefault(x => 
                                x.CustomerID == objData.CustomerID);
                if (r != null)
                {
                    #region
                    r.Title = objData.Title;
                    r.Name = objData.Name;
                    r.Lastname = objData.Lastname;
                    r.Address = objData.Address;
                    r.Location = objData.Location;
                    r.Tel = objData.Tel;
                    r.ModifiedBy = 0;
                    r.ModifiedDate = DateTime.Now;
                    objResult = r;
                    _context.CustomerRegister.Update(r);
                    _context.SaveChanges();
                    #endregion
                }
                return Ok();
            }
            catch (Exception ex)
            {
                EventStatus = ex.Message;
                return BadRequest(ex.Message);
            }
            finally
            {
                #region
                TransactionLog log = new TransactionLog();
                log.ActivityType = "Profile";
                log.EntityName = "UpdateCustomer";
                log.EntityTypeID = null;
                log.ReferID = null;
                log.RequestData = Newtonsoft.Json.JsonConvert.SerializeObject(objData);
                log.ResultDetail = Newtonsoft.Json.JsonConvert.SerializeObject(objResult);
                log.Detail = EventStatus;
                log.Result = EventStatus == "" ? "success" : "error";
                transactionLogService.LogInsert(log);
                #endregion
            }
        }


    }
}
