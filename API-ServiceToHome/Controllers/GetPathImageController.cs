﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_ServiceToHome.Data;
using API_ServiceToHome.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API_ServiceToHome.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class GetPathImageController : ControllerBase
    {
        private readonly DBContext _context;
        private IFactoryService factoryService;

        public GetPathImageController(DBContext context, IFactoryService factoryService)
        {
            _context = context;
            this.factoryService = factoryService;
        }

        //private readonly IWebHostEnvironment _env;


        // GET: api/GetPathImage/5
        [HttpGet("{id}", Name = "GetGetPathImage")]
        public IEnumerable<ImagePath> GetGetPathImage(int id)
        {
            List<ImagePath> listpath = new List<ImagePath>();
            string base_url = factoryService.GetAppSetting("Common", "ViewFileUrl");  ////@"https://apisvth.wherey.co/api/UploadAndPreview/";
            factoryService.GetAppSetting("Common", "ViewFileUrl");
            var result = from attc in _context.AttachmentFile
                         where attc.ReferID == id && attc.EntityTypeID == 1
                         select new { attc.FilePath };

            foreach (var item in result)
            {
                ImagePath img_paht = new ImagePath();
                img_paht.FilePath = base_url + item.FilePath;
                listpath.Add(img_paht);
            }
            
            return listpath;
            //return result.ToList();
        }
    }
}
