﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_ServiceToHome.Data;
using API_ServiceToHome.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API_ServiceToHome.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class CategoryController : ControllerBase
    {
        readonly DBContext _context;
        public CategoryController(DBContext context)
        {
            _context = context;
        }
        // GET: api/Category
        [HttpGet]
        public IEnumerable<Category> GetCategory()
        {
            return _context.Category.ToList();
        }

        // GET: api/Category/5
        [HttpGet("{id}", Name = "GetCategory")]
        public Category GetCategory(int id)
        {
            return _context.Category.FirstOrDefault(o => o.CategoryID == id);
        }

        // POST: api/Category
        [HttpPost]
        public void Post([FromBody] Category value)
        {
            _context.Category.Add(value);
            _context.SaveChanges();
        }

    }
}
