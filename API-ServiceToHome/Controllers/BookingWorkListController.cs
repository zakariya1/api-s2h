﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using API_ServiceToHome.Data;
using API_ServiceToHome.Models.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API_ServiceToHome.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class BookingWorkListController : ControllerBase
    {
        private readonly DBContext _context;

        public BookingWorkListController(DBContext context)
        {
            _context = context;
        }

        [HttpPost("BookingWorklist/{bookId}", Name = "BookingWorklist")]
        public IActionResult BookingWorklist(int bookId)
        {
            if (bookId == null)
            {
                return NotFound();
            }
            try
            {
                //List<WorkflowProcessBookingModel> model = new List<WorkflowProcessBookingModel>();
                var result = _context.ViewWorkflowProcessBooking.Where(x => x.BookID == bookId).OrderByDescending(x => x.CreatedDate).ToList();
                //foreach (var item in result)
                //{
                //    DateTime? From = DateTime.ParseExact(item.ActionDate.ToString(), "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None);
                //    //WorkflowProcessBookingModel md = new WorkflowProcessBookingModel();
                //    //item.ActionDate = DateTime.ParseExact(item.ActionDate, "d/M/yyyy HH:mm", CultureInfo.InvariantCulture);
                //    //md.ActionDate = String.Format("{0:d/M/yyyy HH:mm:ss}", item.ActionDate);
                //    model.Add(item);

                //}
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
