﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_ServiceToHome.Data;
using API_ServiceToHome.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API_ServiceToHome.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class RoleController : ControllerBase
    {
        readonly DBContext _context;
        public RoleController(DBContext context)
        {
            _context = context;
        }
        // GET: api/Role
        [HttpGet]
        public IEnumerable<RoleUser> GetRole()
        {
            return _context.Role.ToList();
        }

        // GET: api/Role/5
        [HttpGet("{id}", Name = "GetRole")]
        public RoleUser GetRole(int id)
        {
            return _context.Role.FirstOrDefault(o => o.RoleID == id);
        }

        // POST: api/Role
        [HttpPost]
        public void Post([FromBody] RoleUser value)
        {
            _context.Role.Add(value);
            _context.SaveChanges();

        }
    }
}
