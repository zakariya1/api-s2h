﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_ServiceToHome.Data;
using API_ServiceToHome.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API_ServiceToHome.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class MasterCancelReasonController : ControllerBase
    {
        readonly DBContext dBContext;
        public MasterCancelReasonController(DBContext context)
        {
            dBContext = context;
        }

        // GET: api/MasterCancelReason
        [HttpGet]
        public IEnumerable<MasterCancelReason> Get(int UserTypeID)
        {
            return dBContext.MasterCancelReason
                            .Where(x => x.UserTypeID == UserTypeID)
                            .ToList();
        }

        //// GET: api/MasterCancelReason/5
        //[HttpGet("{id}", Name = "Get")]
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST: api/MasterCancelReason
        //[HttpPost]
        //public void Post([FromBody] string value)
        //{
        //}

        //// PUT: api/MasterCancelReason/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        //// DELETE: api/ApiWithActions/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
