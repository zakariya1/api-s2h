﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using API_ServiceToHome.Data;
using API_ServiceToHome.Models;
using API_ServiceToHome.Models.InputModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace API_ServiceToHome.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class UploadAndPreviewController : ControllerBase
    {
        private string EventStatus { get; set; } = "";
        private readonly DBContext _context;
        private IAttachmentService attachmentService;
        private IFactoryService factoryService;
        private IConfiguration _configuration;
        private IOptions<AppSettingsModel> settings;
        public string[] supportedTypes = new[] { "png", "jpg", "jpeg" }; //"bmp", "emf", "exif", "gif", "tiff", "icon", "memoryBmp", "wmf" };
        private ITransactionLogService transactionLogService;
        public UploadAndPreviewController(DBContext context, IAttachmentService attachmentService
            , IFactoryService factoryService, ITransactionLogService log)
        {
            _context = context;
            this.attachmentService = attachmentService;
            this.factoryService = factoryService;
            this.transactionLogService = log;
        }

        // GET: api/UploadAndPreview
        [HttpGet]
        public IEnumerable<AttachmentFile> Get()
        {

            return _context.AttachmentFile.ToList();
        }

        // GET: api/UploadAndPreview/5
        [HttpGet("{guid}", Name = "GetUpload")]
        public IActionResult GetUpload(string guid = "")
        {
            try
            {
                if (guid != null)
                {      
                    return File(attachmentService.Viewfile(guid), "image/jpeg");
                }
                return BadRequest();
            }
            catch (Exception ex)
            {
                return BadRequest(new { status = false, message = "Error: " + ex.Message });
            }
            
        }

        // POST: api/UploadAndPreview
        [HttpPost("MultiFileImagesUpload", Name = "MultiFileImagesUpload")]
        public ActionResult MultiFileImagesUpload([FromForm] FileInputMultiImage objData)
        {
            var _objData = objData;
            try
            {
                var result = attachmentService.SaveServiceCardImages(objData);
                return Ok(result);
            }
            catch (Exception ex)
            {
                EventStatus = ex.Message;
                return BadRequest(ex.Message);
            }
            finally
            {
                TransactionLog log = new TransactionLog();
                log.ActivityType = "UploadFile";
                log.EntityName = "MultipleFileUpload";
                log.EntityTypeID = null;
                log.ReferID = objData.ReferID.ToString() == "" ? "" : objData.ReferID.ToString(); ;
                log.RequestData = Newtonsoft.Json.JsonConvert.SerializeObject(_objData);
                log.ResultDetail = Newtonsoft.Json.JsonConvert.SerializeObject(objData);
                log.Detail = EventStatus;
                log.Result = EventStatus == "" ? "success" : "error";
                transactionLogService.LogInsert(log);
            }
        }
        [HttpPost("SigleFileImagesUpload", Name = "SigleFileImagesUpload")]
        public ActionResult SigleFileUpload([FromForm]FileInputSingleImage objData)
        {
            var result = (dynamic)null;
            try
            {
                result = attachmentService.SaveProfileImage(objData);
                return Ok(result);
            }
            catch (Exception ex)
            {
                EventStatus = ex.Message;
                return BadRequest(ex.Message);
            }
            finally
            {
                TransactionLog log = new TransactionLog();
                log.ActivityType = "UploadFile";
                log.EntityName = "SingleFileUpload";
                log.EntityTypeID = null;
                log.ReferID = objData.ReferID.ToString() == "" ? "" : objData.ReferID.ToString(); ;
                log.RequestData = Newtonsoft.Json.JsonConvert.SerializeObject(objData);
                log.ResultDetail = Newtonsoft.Json.JsonConvert.SerializeObject(result);
                log.Detail = EventStatus;
                log.Result = EventStatus == "" ? "success" : "error";
                transactionLogService.LogInsert(log);
            }
        }
    }
}
