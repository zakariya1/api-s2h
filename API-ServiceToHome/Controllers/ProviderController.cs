﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_ServiceToHome.Data;
using API_ServiceToHome.Models;
using API_ServiceToHome.Models.InputModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API_ServiceToHome.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProviderController : ControllerBase
    {
        private string EventStatus { get; set; } = "";
        private ITransactionLogService transactionLogService;
        private IProviderService providerService;

        public ProviderController(ITransactionLogService transactionLogService, IProviderService providerService)
        {
            this.transactionLogService = transactionLogService;
            this.providerService = providerService;
        }

        [HttpPost("StartTracking", Name = "ProviderStartTracking")]
        public IActionResult ProviderStartTracking([FromBody] TrackingInputModel value)
        {
            var objData = value;
            try
            {
                if (value == null)
                {
                    return NotFound();
                }

                var result = providerService.StartTrackingProvider(value);

                return Ok(new
                {
                    msg = "Save Tracking Completed",
                    status = true,
                    data = result
                });
            }
            catch (Exception ex)
            {
                EventStatus = ex.Message;
                return BadRequest(ex.Message);
            }
            finally
            {
                #region 
                TransactionLog log = new TransactionLog();
                log.ActivityType = "Tracking";
                log.EntityName = "ProviderStartTracking";
                log.EntityTypeID = null;
                log.ReferID = null;
                log.RequestData = Newtonsoft.Json.JsonConvert.SerializeObject(objData);
                log.ResultDetail = Newtonsoft.Json.JsonConvert.SerializeObject(value);
                log.Detail = EventStatus;
                log.Result = EventStatus == "" ? "success" : "error";
                transactionLogService.LogInsert(log);
                #endregion
            }
        }
        [HttpPost("EndTracking", Name = "ProviderEndTracking")]
        public IActionResult ProviderEndTracking([FromBody] TrackingInputModel value)
        {
            var objData = value;
            try
            {
                if (value == null)
                {
                    return NotFound();
                }

                var result = providerService.EndTrackingProvider(value);

                return Ok(new
                {
                    msg = "Save Tracking Completed",
                    status = true,
                    data = result
                });
            }
            catch (Exception ex)
            {
                EventStatus = ex.Message;
                return BadRequest(ex.Message);
            }
            finally
            {
                #region
                TransactionLog log = new TransactionLog();
                log.ActivityType = "Tracking";
                log.EntityName = "ProviderEndTracking";
                log.EntityTypeID = null;
                log.ReferID = null;
                log.RequestData = Newtonsoft.Json.JsonConvert.SerializeObject(objData);
                log.ResultDetail = Newtonsoft.Json.JsonConvert.SerializeObject(value);
                log.Detail = EventStatus;
                log.Result = EventStatus == "" ? "success" : "error";
                transactionLogService.LogInsert(log);
                #endregion
            }
        }

        [HttpGet("GetLocationTracking", Name = "GetLocationTracking")]
        public IActionResult GetLocationTracking(int? BookID = 0)
        {
            var objResult = (dynamic)null;
            try
            {
                if (BookID == 0)
                {
                    return NotFound();
                }

                objResult = providerService.GetLocationTracking(BookID);
                return Ok(new
                {
                    msg = "Result Location Tracking",
                    status = true,
                    data = objResult
                });
            }
            catch (Exception ex)
            {
                EventStatus = ex.Message;
                return BadRequest(ex.Message);
            }
            finally
            {
                #region
                TransactionLog log = new TransactionLog();
                log.ActivityType = "Tracking";
                log.EntityName = "GetLocationTracking";
                log.EntityTypeID = null;
                log.ReferID = BookID.ToString();
                log.RequestData = BookID.ToString();
                log.ResultDetail = Newtonsoft.Json.JsonConvert.SerializeObject(objResult);
                log.Detail = EventStatus == "" ? null : EventStatus;
                log.Result = EventStatus == "" ? "success" : "error";
                transactionLogService.LogInsert(log);
                #endregion
            }
        }
    }
}
