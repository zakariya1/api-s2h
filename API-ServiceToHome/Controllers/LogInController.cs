﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using API_ServiceToHome.Data;
using API_ServiceToHome.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace API_ServiceToHome.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class LogInController : ControllerBase
    {
        private string EventStatus { get; set; } = "";
        private IAuthen authen;
        private ITransactionLogService transactionLogService;

        public LogInController(IAuthen authen, ITransactionLogService logTransaction)
        {
 
            this.authen = authen;
            this.transactionLogService = logTransaction;
        }

        // POST: api/LogIn
        [HttpPost("LogInCustomer", Name = "LogInCustomer")]
        public IActionResult LoginCustomer([FromBody] Login value)
        {
            var customer = (dynamic)null;
            try
            {
                customer = authen.CustomerLogin(value);
                if (customer.UserGUID != null)
                {
                    if (customer.isVerify)
                    {
                        return Ok(new { msg = "success", status = true, data = JsonSerializer.Serialize(customer) });
                    }
                    else
                    {
                        return Ok(new { msg = "active email", status = true, data = JsonSerializer.Serialize(customer) });
                    }
                }
                else
                {
                    return Ok(new { msg = "fail", status = false, data = "" });
                }
            }
            catch (Exception ex)
            {
                EventStatus = ex.Message;
                return BadRequest(ex.Message);
            }
            finally
            {
                TransactionLog log = new TransactionLog();
                log.ActivityType = "Authen";
                log.EntityName = "LoginCustomer";
                log.EntityTypeID = null;
                log.ReferID = null;
                log.RequestData = Newtonsoft.Json.JsonConvert.SerializeObject(value);
                log.ResultDetail = Newtonsoft.Json.JsonConvert.SerializeObject(customer);
                log.Detail = EventStatus;
                log.Result = EventStatus == "" ? "success" : "error";
                transactionLogService.LogInsert(log);
            }
        }
        [HttpPost("LogInProvider", Name = "LogInProvider")]
        public IActionResult LogInProvider([FromBody] Login value)
        {
            var provider = (dynamic)null;
            try
            {
                provider = authen.ProviderLogin(value);
                if (provider.UserGUID != null)
                {
                    if (provider.isVerify)
                    {
                        return Ok(new { msg = "success", status = true, data = JsonSerializer.Serialize(provider) });
                    }
                    else
                    {
                        return Ok(new { msg = "active email", status = true, data = JsonSerializer.Serialize(provider) });
                    }
                }
                else
                {
                    return Ok(new  { msg = "fail",  status = false, data = "" });
                }
            }
            catch (Exception ex)
            {
                EventStatus = ex.Message;
                return BadRequest(ex.Message);
            }
            finally
            {
                TransactionLog log = new TransactionLog();
                log.ActivityType = "Authen";
                log.EntityName = "LoginProvider";
                log.EntityTypeID = null;
                log.ReferID = null;
                log.RequestData = Newtonsoft.Json.JsonConvert.SerializeObject(value);
                log.ResultDetail = Newtonsoft.Json.JsonConvert.SerializeObject(provider);
                log.Detail = EventStatus;
                log.Result = EventStatus == "" ? "success" : "error";
                transactionLogService.LogInsert(log);
            }
        }
    }
}
