﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using API_ServiceToHome.Data;
using API_ServiceToHome.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API_ServiceToHome.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Consumes("application/json","application/json-patch+json","multipart/form-data")]
    [Consumes("multipart/form-data")]
    public class FileUploadImgController : ControllerBase
    {
        private readonly DBContext _context;
        //private readonly IWebHostEnvironment _env;

        public FileUploadImgController(DBContext context)
        {
            //_env = env;
            _context = context;
        }

        // GET: api/FileUploadImg/5
        //[HttpGet("{id}", Name = "GetFileUpload")]
        //public string GetFileUpload(int id)
        //{
        //    return "value";
        //}
         //POST: api/FileUploadImg
        //[HttpPost]
       // public ActionResult Post([FromForm] FileInputImage value)
        //{

            //string hostDirectory = @"D:/Inetpub/vhosts/wherey.co/apisvth.wherey.co/uploads";
            ////string DirectoryPath = @"C:\\FileUpload"; //D:/Inetpub/vhosts/wherey.co/apisvth.wherey.co

            //string path = string.Empty;
            //string filePath = string.Empty;
            //string Identifier = string.Empty;
            //string entitytype = string.Empty;
            //string subfolder = string.Empty;
            //string fullPath = string.Empty;

            //if (value.EntityType == Helper.EnumHelper.EntityType.SVCARD)
            //{
            //    entitytype = Helper.EnumHelper.EntityType.SVCARD;
            //    subfolder = @"/CardService";
            //}
            //else
            //{
            //    entitytype = Helper.EnumHelper.EntityType.SVCARD;
            //    subfolder = @"/Profiles";
            //}

            //path = hostDirectory + subfolder;
            //if (!Directory.Exists(path))
            //{
            //    Directory.CreateDirectory(path);
            //}
            //foreach (var file in value.Files)
            //{
            //    Identifier = Guid.NewGuid().ToString();
            //    string fileName = Identifier + "." + Path.GetFileName(file.ContentType);
            //    filePath = subfolder + @"/" + fileName;
            //    fullPath = path + @"/" + fileName;
            //    using (FileStream stream = new FileStream(Path.Combine(path, fileName), FileMode.Create))
            //    {
            //        file.CopyTo(stream);

            //        AttachmentFile atf = new AttachmentFile();
            //        atf.EntityID = value.EntityID;
            //        atf.EntityType = entitytype;
            //        atf.Identifier = Identifier;
            //        atf.FilePath = filePath;
            //        atf.FileName = file.FileName;
            //        atf.SaveFileName = fullPath;
            //        atf.FileSize = file.Length;
            //        atf.Type = file.ContentType;
            //        atf.CreatedDate = DateTime.Now;
            //        atf.CreatedBy = 0;
            //        atf.ModifiedDate = DateTime.Now;
            //        atf.ModifiedBy = 0;

            //        _context.AttachmentFile.Add(atf);
            //        _context.SaveChanges();
            //    }
            //}

            //return Ok(new { status = true, message = "Student Posted Successfully" });

       // }


        // GET: api/FileUploadImg/1
        //[HttpGet("{id}",Name = "GetViewImage")]
        //public ActionResult GetViewImage(int id)
        //{
        //    string hostDirectory = @"D:/Inetpub/vhosts/wherey.co/apisvth.wherey.co/Uploads";
        //    // string fullpath = @"D:\Arise\API-ServiceToHome\API-ServiceToHome\Uploads\CardService";
        //    //string path = Path.Combine(@contentRootPath, @"Uploads\CardService");
        //    //var stream = new FileStream(Path.Combine(fullpath,filename), FileMode.Open, FileAccess.Read);
        //    //return File(stream, "image/png", filename);

        //    Byte[] b = System.IO.File.ReadAllBytes(Path.Combine(hostDirectory, id.ToString()));   // You can use your own method over here.         
        //    return File(b, "image/jpeg");
        //}

        // PUT: api/FileUploadImg/5
        //[HttpPut("{id}")]
        //public ActionResult Put(int id, IFormFile file)
        //{
        //    try
        //    {

        //        string hostDirectory = @"D:/Inetpub/vhosts/wherey.co/apisvth.wherey.co/";
        //        //string DirectoryPath = @"C:\\FileUpload"; //D:/Inetpub/vhosts/wherey.co/apisvth.wherey.co
        //        string path = Path.Combine(hostDirectory, @"Uploads/CardService");
        //        string filePath = string.Empty;
        //        string Identifier = string.Empty;
        //        //if (!Directory.Exists(path))
        //        //{
        //        //    Directory.CreateDirectory(path);
        //        //}
        //        Identifier = Guid.NewGuid().ToString();
        //        string fileName = Identifier + "." + Path.GetFileName(file.ContentType);
        //        filePath = path + @"/" + fileName;



        //        using (FileStream stream = new FileStream(Path.Combine(path, fileName), FileMode.Create))
        //        {
        //            file.CopyTo(stream);

        //            AttachmentFile atf = new AttachmentFile();
        //            atf.EntityID = id;
        //            atf.EntityType = Helper.EnumHelper.EntityType.SVCARD;
        //            atf.Identifier = "";
        //            atf.FilePath = filePath;
        //            atf.FileName = file.Name;
        //            atf.SaveFileName = filePath;
        //            atf.FileSize = file.Length;
        //            atf.Type = file.ContentType;
        //            atf.CreatedDate = DateTime.Now;
        //            atf.CreatedBy = 0;
        //            atf.ModifiedDate = DateTime.Now;
        //            atf.ModifiedBy = 0;

        //            _context.AttachmentFile.Add(atf);
        //            _context.SaveChanges();
        //        }

        //        return Ok(new { status = true, message = "Student Posted Successfully" });
        //        //////string hostDirectory = @"D:\\Inetpub\vhosts\wherey.co\apisvth.wherey.co\";
        //        ////////string DirectoryPath = @"C:\\FileUpload"; //D:/Inetpub/vhosts/wherey.co/apisvth.wherey.co
        //        //////string path = Path.Combine(hostDirectory, @"Uploads\CardService");
        //        //////string filePath = string.Empty;
        //        //////string Identifier = string.Empty;
        //        //////if (!Directory.Exists(path))
        //        //////{
        //        //////    Directory.CreateDirectory(path);
        //        //////}
        //        //////Identifier = Guid.NewGuid().ToString();
        //        //////string fileName = Identifier + "." + Path.GetFileName(files.ContentType);
        //        //////filePath = path + @"\" + fileName;

        //        //////AttachmentFile atf = new AttachmentFile();
        //        //////atf.EntityID = id;
        //        //////atf.EntityType = Helper.EnumHelper.EntityType.SVCARD;
        //        //////atf.Identifier = Identifier;
        //        //////atf.FilePath = filePath;
        //        //////atf.FileName = files.Name;
        //        //////atf.SaveFileName = filePath;
        //        //////atf.FileSize = files.Length;
        //        //////atf.Type = files.ContentType;
        //        //////atf.CreatedDate = DateTime.Now;
        //        //////atf.CreatedBy = 0;
        //        //////atf.ModifiedDate = DateTime.Now;
        //        //////atf.ModifiedBy = 0;

        //        //////_context.AttachmentFile.Add(atf);
        //        //////_context.SaveChanges();

        //        //////using (FileStream stream = new FileStream(Path.Combine(path, fileName), FileMode.Create))
        //        //////{
        //        //////    files.CopyTo(stream);                  
        //        //////}

        //        //////return Ok(new { status = true, message = "Student Posted Successfully" });
        //    }
        //    catch (Exception ex)
        //    {
        //        return BadRequest(new { status = false, message = ex.ToString() });
        //    }
        //}
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
