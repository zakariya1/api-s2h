﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API_ServiceToHome.Data;
using API_ServiceToHome.Helper;
using API_ServiceToHome.Models;
using API_ServiceToHome.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace API_ServiceToHome.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class ProviderRegisterController : ControllerBase
    {
        private string EventStatus { get; set; } = "";
        private readonly DBContext _context;
        private readonly IEmailService email;
        private IFactoryService factoryService;
        private ITransactionLogService transactionLogService;

        public ProviderRegisterController(DBContext context, IEmailService email, IFactoryService factoryService, ITransactionLogService transactionLogService)
        {
            _context = context;
            this.email = email;
            this.factoryService = factoryService;
            this.transactionLogService = transactionLogService;
        }
        // GET: api/ProviderRegister
        [HttpGet]
        public IEnumerable<ProviderRegister> GetProviderRegister()
        {
            return _context.ProviderRegister
                .ToList();
        }

        // GET: api/ProviderRegister/5
        [HttpGet("{id}", Name = "GetProviderRegister")]
        public ProviderRegister GetProviderRegister(int id)
        {
            return _context.ProviderRegister
                .Where(o => o.ProviderID == id).FirstOrDefault();
        }

        // POST: api/ProviderRegister
        [HttpPost]
        public IActionResult Post([FromBody] ProviderRegister value)
        {
            //var objData = value;
            ProviderRegister result = null; //overkill
            string EntityName = string.Empty;
            string DirectoryPath = factoryService.GetAppSetting("Common", "ViewFileUrl");
            try
            {
                if (value == null)
                {
                    return NotFound();
                }

                int _profileProvider = (int)Helper.EnumHelper.EntityType.PROFILE_PROVIDER;
                if (value.RegisterType == (int)Helper.EnumHelper.RegisType.Normal)
                {
                    EntityName = Helper.EnumHelper.RegisType.Normal.ToString();
                    result = _context.ProviderRegister.Where(o => o.Email == value.Email
                    && o.RegisterType == (int)Helper.EnumHelper.RegisType.Normal).FirstOrDefault();
                    if (result != null)
                    {
                        var img_url = _context.AttachmentFile.OrderByDescending(x => x.CreatedDate)
                                      .FirstOrDefault(o => o.ReferID == result.ProviderID
                                      && o.EntityTypeID == _profileProvider);

                        if (img_url != null && img_url.FilePath != null)
                            result.profile_img = DirectoryPath + img_url.FilePath;
                    }
                }
                else if (value.RegisterType == (int)Helper.EnumHelper.RegisType.Google)
                {
                    EntityName = Helper.EnumHelper.RegisType.Google.ToString();
                    result = _context.ProviderRegister.Where(o => o.Email == value.Email
                    && o.RegisterType == (int)Helper.EnumHelper.RegisType.Google).FirstOrDefault();
                    if (result != null)
                    {
                        var img_url = _context.AttachmentFile.OrderByDescending(x => x.CreatedDate)
                                    .FirstOrDefault(o => o.ReferID == result.ProviderID
                                    && o.EntityTypeID == _profileProvider);

                        if (img_url != null && img_url.FilePath != null)
                            result.profile_img = DirectoryPath + img_url.FilePath;
                    }
                }
                else if (value.RegisterType == (int)Helper.EnumHelper.RegisType.Apple)
                {
                    EntityName = Helper.EnumHelper.RegisType.Apple.ToString();
                    result = _context.ProviderRegister.Where(o => o.TokenId.ToLower() == value.TokenId.ToLower()
                    && o.RegisterType == (int)Helper.EnumHelper.RegisType.Apple).FirstOrDefault();
                    if (result != null)
                    {
                        var img_url = _context.AttachmentFile.OrderByDescending(x => x.CreatedDate)
                                    .FirstOrDefault(o => o.ReferID == result.ProviderID
                                    && o.EntityTypeID == _profileProvider);
                        if (img_url != null && img_url.FilePath != null)
                            result.profile_img = DirectoryPath + img_url.FilePath;
                    }

                    if (string.IsNullOrEmpty(value.TokenId))
                    {
                        if (result != null)
                        {
                            return Ok(new { msg = "data duplicate", status = true, data = result });
                        }
                        return Ok(new { msg = "sign in with Apple not working", status = true, data = "" });
                    }
                }
                else if (value.RegisterType == (int)Helper.EnumHelper.RegisType.Facebook)
                {
                    EntityName = Helper.EnumHelper.RegisType.Facebook.ToString();
                    result = _context.ProviderRegister
                        .Where(o => o.RegisterType == (int)Helper.EnumHelper.RegisType.Facebook
                        && o.TokenId.ToLower() == value.TokenId.ToLower()
                        ).FirstOrDefault();

                    if (result != null)
                    {
                        var img_url = _context.AttachmentFile.OrderByDescending(x => x.CreatedDate)
                                    .FirstOrDefault(o => o.ReferID == result.ProviderID
                                    && o.EntityTypeID == _profileProvider);

                        if (img_url != null && img_url.FilePath != null)
                            result.profile_img = DirectoryPath + img_url.FilePath;
                    }

                }
                else
                {
                    return Ok(new
                    {
                        msg = "please key id register type!",
                        status = true,
                        data = ""
                    });
                }

                if (result == null)
                {
                    using (var transaction = _context.Database.BeginTransaction())
                    {
                        GenerateCode gen = new GenerateCode(_context);
                        value.isActive = true;
                        value.isVerify = value.RegisterType == (int)Helper.EnumHelper.RegisType.Normal ? false : true;
                        value.Username = value.Email;
                        value.Password = value.Password == null ? "" : SecurityHelper.GetMD5(value.Password);
                        value.CreatedBy = 0;
                        value.CreatedDate = DateTime.Now;
                        value.ProviderCode = gen.CreateCode(Helper.EnumHelper.CodeTypeName.PROVIDER);
                        value.UserGUID = Guid.NewGuid().ToString();


                        _context.ProviderRegister.Add(value);
                        _context.SaveChanges();

                        result = value;

                        if (!string.IsNullOrEmpty(value.Email))
                        {
                            #region
                            //Email m = new Email();
                            //string UrlActivate = settings.Value.BaseUrlMailActive + value.UserGUID;
                            //m.Subject = "Please verify your account";
                            //m.Body = $"please your click url {UrlActivate}. thak you.";
                            //m.To = value.Email;
                            //bool flag = email.SendEmailCustomerActive(m);

                            //Email m = new Email();
                            //string UrlActivate = factoryService.GetAppSetting("Email", "UrlActiveProvider") + value.UserGUID;

                            //StringBuilder sb = new StringBuilder();
                            //sb.AppendLine("<h1>Thank you for signing up with us.</h1> <br><br>");
                            //sb.AppendLine("Please <b>verify</b> your email address by clicking the link below: <br>");
                            //sb.AppendLine($"<h2><font color=#336699><b><a href = '{UrlActivate}'>Verify email address</a></b></font></h2> <br><br><br><br>");
                            //sb.AppendLine("if you have any questions, just contact us.<br>");
                            //sb.AppendLine("<img src = 'https://apisvth.wherey.co/api/UploadAndPreview/20200525ORTHOR_9669a647-810f-4ec4-9638-ae91d72bed20.png' style='width: 15px; height: 15px; '>");
                            //sb.AppendLine("<font color=#336699><b><a href = 'https://line.me/R/ti/p/%40716aeeml'>Line</a></b></font><br>");
                            //sb.AppendLine("<img src = 'https://apisvth.wherey.co/api/UploadAndPreview/20200525ORTHOR_504ac020-71ae-45eb-b924-0d31254547d3.png' style='width: 15px; height: 15px; '>");
                            //sb.AppendLine("<font color=#336699><b><a href = 'https://line.me/R/ti/p/%40716aeeml'>Facebook</a></b></font><br>");
                            //sb.AppendLine("<img src = 'https://apisvth.wherey.co/api/UploadAndPreview/20200525ORTHOR_2ef3e045-093b-4b24-885b-57868f31f81f.png' style='width: 15px; height: 15px; '>");
                            //sb.AppendLine("<font color=#336699><b><a href = 'https://line.me/R/ti/p/%40716aeeml'>Website</a></b></font><br><br>");
                            //sb.AppendLine("Best regards, <br>Serve Service Team.");

                            //m.Subject = "Please verify your email address.";
                            //m.Body = sb.ToString();
                            ////m.Body = $"please your click url {UrlActivate}. thak you.";
                            //m.To = value.Email;
                            #endregion
                            if (value.RegisterType == (int)Helper.EnumHelper.RegisType.Normal)
                            {
                                bool flag = email.SendEmailActive(value.UserGUID, value.Email, Helper.EnumHelper.CodeTypeUse.PROVIDER);
                            }
                        }

                        transaction.Commit();
                    }

                    return Ok(new
                    {
                        msg = "regis success",
                        status = true,
                        data = result
                    });
                }
                else
                {
                    if (result.isActive == false && result.RegisterType == (int)Helper.EnumHelper.RegisType.Normal)
                    {
                        return Ok(new
                        {
                            msg = "active email",
                            status = true,
                            data = result
                        });
                    }
                    else
                    {
                        return Ok(new
                        {
                            msg = "data duplicate",
                            status = true,
                            data = result
                        });
                    }

                }
            }
            catch (Exception ex)
            {
                EventStatus = ex.Message;
                return BadRequest(ex.ToString());
            }
            finally
            {
                #region
                TransactionLog log = new TransactionLog();
                log.ActivityType = "AuthenProvider";
                log.EntityName = EntityName;
                log.EntityTypeID = null;
                log.ReferID = result.ProviderID.ToString();
                log.RequestData = Newtonsoft.Json.JsonConvert.SerializeObject(value);
                log.ResultDetail = Newtonsoft.Json.JsonConvert.SerializeObject(result);
                log.Detail = Newtonsoft.Json.JsonConvert.SerializeObject(EventStatus); ;
                log.Result = EventStatus == "" ? "success" : "error";
                transactionLogService.LogInsert(log);
                #endregion
            }
        }
    }
}
