﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_ServiceToHome.Data;
using API_ServiceToHome.Helper;
using API_ServiceToHome.Models;
using API_ServiceToHome.Models.InputModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API_ServiceToHome.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class AuthenController : ControllerBase
    {
        private string EventStatus { get; set; } = "";
        private readonly DBContext _context;
        private ITransactionLogService transactionLogService;

        public AuthenController(DBContext context, ITransactionLogService transactionLogService)
        {
            _context = context;
            this.transactionLogService = transactionLogService;
        }

        // POST: api/Authen
        [HttpPost("ResetGenerateNewPassword", Name = "ResetGenerateNewPassword")]
        public IActionResult ResetGeneratePassword([FromBody] ResetPasswordModel objData)
        {
            if (objData == null && objData.Email == null && (objData.UserType == null || objData.UserType == 0))
            {
                return Ok(new
                {
                    msg = "data not found!",
                    status = false,
                    data = ""
                });
            }
            var result = (dynamic)null;
            try
            {
                if (objData.UserType == (int)Helper.EnumHelper.UserType.Customer)
                {
                    CustomerRegister customer = _context.CustomerRegister.FirstOrDefault(
                        o => o.Email == objData.Email && o.RegisterType == (int)Helper.EnumHelper.RegisType.Normal);
                    if (customer != null)
                    {
                        if (objData.NewPassword.Length <= 7)
                        {
                            return Ok(new
                            {
                                msg = "Please enter new password atleast 8 digit",
                                status = false,
                                data = ""
                            });
                        }
                        customer.Password = SecurityHelper.GetMD5(objData.NewPassword).ToLower();
                        customer.ModifiedDate = DateTime.Now;
                        _context.CustomerRegister.Update(customer);
                        _context.SaveChanges();
                        result = customer;

                    }
                    else {
                        return Ok(new
                        {
                            msg = "Email customer not found!",
                            status = false,
                            data = ""
                        }) ;
                    }
                }
                else if(objData.UserType == (int)Helper.EnumHelper.UserType.Provider) 
                {
                    ProviderRegister provider = _context.ProviderRegister.FirstOrDefault(
                                o => o.Email == objData.Email && o.RegisterType == (int)Helper.EnumHelper.RegisType.Normal);
                    if (provider != null)
                    {

                        if (objData.NewPassword.Length <= 7)
                        {
                            return Ok(new
                            { 
                                msg = "Please enter new password atleast 8 digit",
                                status = false,
                                data = ""
                            });
                        }


                        provider.Password = SecurityHelper.GetMD5(objData.NewPassword).ToLower();
                        provider.ModifiedDate = DateTime.Now;
                        _context.ProviderRegister.Update(provider);
                        _context.SaveChanges();
                        result = provider;
                    }
                    else
                    {
                        return Ok(new
                        {
                            msg = "Email provider not found!",
                            status = false,
                            data = ""
                        });
                    }
                }
                return Ok(new
                {
                    msg = "Reset and Generate new password success",
                    status = true,
                    data = result
                });
            }
            catch (Exception ex)
            {
                EventStatus = ex.Message;
                return BadRequest(ex.Message);
            }
            finally
            {
                TransactionLog log = new TransactionLog();
                log.ActivityType = "Authen";
                log.EntityName = "ResetGeneratePassword";
                log.EntityTypeID = null;
                log.ReferID = null;
                log.RequestData = Newtonsoft.Json.JsonConvert.SerializeObject(objData);
                log.ResultDetail = Newtonsoft.Json.JsonConvert.SerializeObject(result);
                log.Detail = EventStatus;
                log.Result = EventStatus == "" ? "success" : "error";
                transactionLogService.LogInsert(log);
            }
        }


    }
}
