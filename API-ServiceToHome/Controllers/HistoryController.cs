﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_ServiceToHome.Data;
using API_ServiceToHome.Models;
using API_ServiceToHome.Models.InputModels;
using API_ServiceToHome.Models.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API_ServiceToHome.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class HistoryController : ControllerBase
    {
        private readonly IHistory BookingHistory;

        public HistoryController(IHistory history)
        {
            this.BookingHistory = history;
        }

        // GET: api/History
        [HttpGet]
        public IEnumerable<HistoryBookingModel> Get()
        {
            return BookingHistory.GetAllBookingHistory();
        }

        [HttpGet("GetHistoryByBookingID{BookingID}", Name = "GetHistoryDetailByBookingID")]
        public HistoryBookingModel GetHistoryDetailByBookingID(int BookingID)
        {
            return BookingHistory.GetHistoryDetailByBookingID(BookingID);
        }

        [HttpPost("GetHistoryByCustID", Name = "GetHistoryByCustID")]
        public IEnumerable<HistoryBookingModel> GetHistoryByCustID(HistoryFilter filters)
        {
            return BookingHistory.GetAllBookingHistoryByCustID(filters);
        }

        //// GET: api/History/5
        //[HttpGet(Name = "GetHistory")]
        //public IActionResult GetHistory()
        //{
        //    return View();
        //}

        //// POST: api/History
        //[HttpPost]
        //public void Post([FromBody] string value)
        //{
        //}

        //// PUT: api/History/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        //// DELETE: api/ApiWithActions/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
