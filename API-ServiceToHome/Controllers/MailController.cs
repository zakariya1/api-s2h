﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API_ServiceToHome.Data;
using API_ServiceToHome.Models;
using API_ServiceToHome.Models.InputModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace API_ServiceToHome.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    public class MailController : ControllerBase
    {

        private string EventStatus { get; set; } = "";
        private readonly IEmailService email;
        private readonly DBContext _context;
        private IFactoryService factoryService;
        private ITransactionLogService transactionLogService;

        public MailController(IEmailService email, DBContext context, IFactoryService factoryService, ITransactionLogService transactionLogService)
        {
            this.email = email;
            _context = context;
            this.factoryService = factoryService;
            this.transactionLogService = transactionLogService;
        }




        // POST: api/Mail
        [HttpPost("SendEmailToCustomerActivate", Name = "SendEmailToCustomerActivate")]
        public IActionResult SendEmailToCustomerActivate([FromBody] MailActivate value)
        {
            var result = (dynamic)null;
            if (value == null)
            {
                return NotFound();
            }
            try
            {
                #region
                //Email m = new Email();
                //string UrlActivate = factoryService.GetAppSetting("Email", "UrlActiveCustomer") + _mail.UserGuid;

                //StringBuilder sb = new StringBuilder();
                //sb.AppendLine("<h1>Thank you for signing up with us.</h1> <br><br>");
                //sb.AppendLine("Please <b>verify</b> your email address by clicking the link below: <br>");
                //sb.AppendLine($"<h2><font color=#336699><b><a href = '{UrlActivate}'>Verify email address</a></b></font></h2> <br><br><br><br>");
                //sb.AppendLine("if you have any questions, just contact us.<br>");
                //sb.AppendLine("<img src = 'https://apisvth.wherey.co/api/UploadAndPreview/20200525ORTHOR_9669a647-810f-4ec4-9638-ae91d72bed20.png' style='width: 15px; height: 15px; '>");
                //sb.AppendLine("<font color=#336699><b><a href = 'https://line.me/R/ti/p/%40716aeeml'>Line</a></b></font><br>");
                //sb.AppendLine("<img src = 'https://apisvth.wherey.co/api/UploadAndPreview/20200525ORTHOR_504ac020-71ae-45eb-b924-0d31254547d3.png' style='width: 15px; height: 15px; '>");
                //sb.AppendLine("<font color=#336699><b><a href = 'https://line.me/R/ti/p/%40716aeeml'>Facebook</a></b></font><br>");
                //sb.AppendLine("<img src = 'https://apisvth.wherey.co/api/UploadAndPreview/20200525ORTHOR_2ef3e045-093b-4b24-885b-57868f31f81f.png' style='width: 15px; height: 15px; '>");
                //sb.AppendLine("<font color=#336699><b><a href = 'https://line.me/R/ti/p/%40716aeeml'>Website</a></b></font><br><br>");
                //sb.AppendLine("Best regards, <br>Serve Service Team.");

                //m.Subject = "Please verify your email address.";
                //m.Body = sb.ToString();
                ////m.Body = $"please your click url {UrlActivate}. thak you.";
                //m.To = _mail.MailTo;
                #endregion

                result = email.SendEmailActive(value.UserGuid, value.MailTo, Helper.EnumHelper.CodeTypeUse.CUSTOMER);
                return Ok(result);
            }
            catch (Exception ex)
            {
                EventStatus = ex.Message;
                return BadRequest(ex.Message);
            }
            finally
            {
                TransactionLog log = new TransactionLog();
                log.ActivityType = "Send Email";
                log.EntityName = "SendEmailActive";
                log.EntityTypeID = null;
                log.ReferID = value.MailTo == "" ? "" : value.MailTo;
                log.RequestData = Newtonsoft.Json.JsonConvert.SerializeObject(value);
                log.ResultDetail = Newtonsoft.Json.JsonConvert.SerializeObject(result);
                log.Detail = Newtonsoft.Json.JsonConvert.SerializeObject(EventStatus);
                log.Result = EventStatus == "" ? "success" : "error";
                transactionLogService.LogInsert(log);
            }
        }
        [HttpPost("SendEmailToProviderActivate", Name = "SendEmailToProviderActivate")]
        public IActionResult SendEmailToProviderActivate([FromBody] MailActivate value)
        {
            var result = (dynamic)null;
            if (value == null)
            {
                return NotFound();
            }
            try
            {
                #region
                //Email m = new Email();
                //string UrlActivate = factoryService.GetAppSetting("Email", "UrlActiveProvider") + _mail.UserGuid;

                //StringBuilder sb = new StringBuilder();
                //sb.AppendLine("<h1>Thank you for signing up with us.</h1> <br><br>");
                //sb.AppendLine("Please <b>verify</b> your email address by clicking the link below: <br>");
                //sb.AppendLine($"<h2><font color=#336699><b><a href = '{UrlActivate}'>Verify email address</a></b></font></h2> <br><br><br><br>");
                //sb.AppendLine("if you have any questions, just contact us.<br>");
                //sb.AppendLine("<img src = 'https://apisvth.wherey.co/api/UploadAndPreview/20200525ORTHOR_9669a647-810f-4ec4-9638-ae91d72bed20.png' style='width: 15px; height: 15px; '>");
                //sb.AppendLine("<font color=#336699><b><a href = 'https://line.me/R/ti/p/%40716aeeml'>Line</a></b></font><br>");
                //sb.AppendLine("<img src = 'https://apisvth.wherey.co/api/UploadAndPreview/20200525ORTHOR_504ac020-71ae-45eb-b924-0d31254547d3.png' style='width: 15px; height: 15px; '>");
                //sb.AppendLine("<font color=#336699><b><a href = 'https://line.me/R/ti/p/%40716aeeml'>Facebook</a></b></font><br>");
                //sb.AppendLine("<img src = 'https://apisvth.wherey.co/api/UploadAndPreview/20200525ORTHOR_2ef3e045-093b-4b24-885b-57868f31f81f.png' style='width: 15px; height: 15px; '>");
                //sb.AppendLine("<font color=#336699><b><a href = 'https://line.me/R/ti/p/%40716aeeml'>Website</a></b></font><br><br>");
                //sb.AppendLine("Best regards, <br>Serve Service Team.");

                //m.Subject = "Please verify your email address.";
                //m.Body = sb.ToString();
                ////m.Body = $"please your click url {UrlActivate}. thak you.";
                //m.To = _mail.MailTo;
                #endregion
                result = email.SendEmailActive(value.UserGuid, value.MailTo, Helper.EnumHelper.CodeTypeUse.PROVIDER);
                return Ok(result);
            }
            catch (Exception ex)
            {
                EventStatus = ex.Message;
                return BadRequest(ex.Message);
            }
            finally
            {
                TransactionLog log = new TransactionLog();
                log.ActivityType = "Email";
                log.EntityName = "SendEmailToProviderActivate";
                log.EntityTypeID = null;
                log.ReferID = value.MailTo == "" ? "" : value.MailTo;
                log.RequestData = Newtonsoft.Json.JsonConvert.SerializeObject(value);
                log.ResultDetail = Newtonsoft.Json.JsonConvert.SerializeObject(result);
                log.Detail = Newtonsoft.Json.JsonConvert.SerializeObject(EventStatus);
                log.Result = EventStatus == "" ? "success" : "error";
                transactionLogService.LogInsert(log);
            }
        }

        [HttpGet("ProviderActivate/{UserGuid}", Name = "ProviderActivateEmail")]
        public IActionResult ProviderActivateEmail(string UserGuid)
        {
            if (string.IsNullOrEmpty(UserGuid))
            {
                return NotFound();
            }
            try
            {
                email.ActivateEmailProvider(UserGuid);

                string hostDirectory = factoryService.GetAppSetting("Common", "RootDirectory");
                var result = _context.AttachmentFile.OrderByDescending(x => x.CreatedDate).FirstOrDefault(x => x.EntityTypeID == 8);
                if (result != null)
                {
                    Byte[] b = System.IO.File.ReadAllBytes(Path.Combine(result.SaveFileName));   // You can use your own method over here.         
                    return File(b, "image/jpeg");
                }

                return Ok("Activate Success");
            }
            catch (Exception ex)
            {
                return Ok("Error :" + ex.Message);
            }
        }

        //[HttpGet("ActivateEmail/{UserGuid}", Name = "CustomerActivateEmail")]
        [HttpGet("CustomerActivate/{UserGuid}", Name = "CustomerActivateEmail")]
        public IActionResult CustomerActivateEmail(string UserGuid)
        {
            if (string.IsNullOrEmpty(UserGuid))
            {
                return NotFound();
            }
            try
            {
                email.ActivateEmailCustomer(UserGuid);

                string hostDirectory = factoryService.GetAppSetting("Common", "RootDirectory");
                var result = _context.AttachmentFile.OrderByDescending(x => x.CreatedDate).FirstOrDefault(x => x.EntityTypeID == 8);
                if (result != null)
                {
                    Byte[] b = System.IO.File.ReadAllBytes(Path.Combine(result.SaveFileName));   // You can use your own method over here.         
                    return File(b, "image/jpeg");
                }

                return Ok("Activate Success");
            }
            catch (Exception ex)
            {
                return Ok("Error :" + ex.Message);
            }
        }
    }
}
