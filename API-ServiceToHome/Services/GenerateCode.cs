﻿using API_ServiceToHome.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Services
{
    public class GenerateCode
    {
        readonly DBContext _context;
        public GenerateCode(DBContext context)
        {
            _context = context;
        }

        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public string  CreateCode(string type)
        {
            string result = string.Empty;

            int ResultID = 0;
            var MaxID = (dynamic)null;
            var TypeCode = (dynamic)null;

            if (type == Helper.EnumHelper.CodeTypeName.PROVIDER)
            {
                 //ResultID = _context.ProviderRegister.Max(x => x.ProviderID);
                ResultID = _context.ProviderRegister.Count();
                if (ResultID != 0)
                {
                    MaxID = Convert.ToInt32(ResultID) + 1;
                }
                TypeCode = Helper.EnumHelper.CodeTypeUse.PROVIDER;
            }
            else if (type == Helper.EnumHelper.CodeTypeName.SERVICECARD)
            {
                ResultID = _context.ServiceCard.Count();
                if (ResultID != 0)
                {
                    MaxID = Convert.ToInt32(ResultID) + 1;
                }
                TypeCode = Helper.EnumHelper.CodeTypeUse.SERVICECARD;
            } 
            else if (type == Helper.EnumHelper.CodeTypeName.CUSTOMER) 
            {
                ResultID = _context.CustomerRegister.Count();
                if (ResultID != 0)
                {
                    MaxID = Convert.ToInt32(ResultID) + 1;
                }
                TypeCode = Helper.EnumHelper.CodeTypeUse.CUSTOMER;
            }
            else if (type == Helper.EnumHelper.CodeTypeName.BUCKET)
            {
                ResultID = _context.CustomerBasket.Count();
                if (ResultID != 0)
                {
                    MaxID = Convert.ToInt32(ResultID) + 1;
                }
                TypeCode = Helper.EnumHelper.CodeTypeUse.BUCKET;
            }
            else if (type == Helper.EnumHelper.CodeTypeName.BOOKING)
            {
                ResultID = _context.Booking.Count();
                if (ResultID != 0)
                {
                    MaxID = Convert.ToInt32(ResultID) + 1;
                }
                TypeCode = Helper.EnumHelper.CodeTypeUse.BOOKING;
            }
            //var output = null; // _context.Provider_register.DefaultIfEmpty().Max(r => r == null ? 0 : r.Id);
            if (ResultID != 0)
            {
                result = TypeCode + DateTime.Now.ToString("yyMM") + MaxID.ToString().PadLeft(4, '0');
                         
            }
            else
            {
                result = TypeCode + DateTime.Now.ToString("yyMM") + "0001";
            }

            return result;


        }  
        

    }
}
