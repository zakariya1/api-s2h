﻿using API_ServiceToHome.Data;

using API_ServiceToHome.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Services
{
    public class CustomerRepo : ICustomerRegister
    {
        readonly DBContext _context;
        public CustomerRepo(DBContext context)
        {
            _context = context;
        }
        public void Delete(CustomerRegister customer)
        {
             _context.CustomerRegister.Remove(customer);
        }

        public List<CustomerRegister> GetAll()
        {
            return _context.CustomerRegister.ToList();
        }

        public CustomerRegister GetById(int id)
        {
            return _context.CustomerRegister.Where(x => x.CustomerID == id).FirstOrDefault(); ;
        }

        public void Insert(CustomerRegister customer)
        {
            _context.CustomerRegister.Add(customer);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public void Update(CustomerRegister customer)
        {
            _context.CustomerRegister.Update(customer);
        }
    }
}
