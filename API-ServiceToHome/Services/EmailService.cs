﻿using API_ServiceToHome.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace API_ServiceToHome.Services
{
    public class EmailService
    {

        public IConfiguration Configuration { get; set; }
        private IOptions<AppSettingsModel> settings;
        public EmailService(IOptions<AppSettingsModel> settings)
        {
            this.settings = settings;
        }

        

        public bool SendMail(Email mail) 
        {
            bool flg = false;
            //string _email  = Configuration.GetValue<string>("MailConfig:EmailAddress");
            //string _display_name = Configuration.GetValue<string>("MailConfig:DispayName");
            //string _password = Configuration.GetValue<string>("MailConfig:Password");

            string _email = settings.Value.EmailAddress;
            string _display_name = settings.Value.EmailDisplayName;
            string _password = settings.Value.EmailPassword;


            try
            {
                MailMessage mm = new MailMessage();
                mm.To.Add(mail.To);
                mm.Subject = mail.Subject;
                mm.Body = mail.Body;
                mm.From = new MailAddress(_email, _display_name);
                mm.IsBodyHtml = false;
                SmtpClient smtp = new SmtpClient("smtp.gmail.com");
                smtp.Port = 587;
                smtp.UseDefaultCredentials = true;
                smtp.EnableSsl = true;
                smtp.Credentials = new System.Net.NetworkCredential(_email, _password);
                smtp.Send(mm);
                flg = true;

            }
            catch(Exception ex) {
                flg = false;
            }

            return flg;
        
        }
    }
}
