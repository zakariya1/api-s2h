﻿using API_ServiceToHome.Data.EFCore;
using API_ServiceToHome.Models;
using API_ServiceToHome.Models.InputModels;
using API_ServiceToHome.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Data
{
    public interface IHistory
    {
        //void Save();
        List<HistoryBookingModel> GetAllBookingHistory();
        List<HistoryBookingModel> GetAllBookingHistoryByCustID(HistoryFilter filters);
        HistoryBookingModel GetHistoryDetailByBookingID(int BookingID);
        
    }
}
