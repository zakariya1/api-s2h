﻿using API_ServiceToHome.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Data
{
    public interface IEmailService
    {
        bool SendEmailActive(string UserGUID, string Email, string CustomerOrProvder);
        void ActivateEmailCustomer(string gUID);
        void ActivateEmailProvider(string gUID);
    }
}
