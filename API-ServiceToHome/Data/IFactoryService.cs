﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Data
{
    public interface IFactoryService
    {
        string GetAppSetting(string ConfigName);
        string GetAppSetting(string Module, string ConfigName);
        string GetDescriptionModuleVariable(string configName);
    }
}
