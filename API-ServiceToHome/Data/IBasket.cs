﻿using API_ServiceToHome.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Data
{
    public interface IBasket
    {
        CustomerBasket CreateBasket(CustomerBasket md);
        void UpdateBasket(CustomerBasket md);
        void RemoveBasket(int id);
        void Save();
    }
}
