﻿using API_ServiceToHome.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Data
{
    public interface IAuthen
    {
        CustomerRegister CustomerLogin(Login login);
        CustomerRegister CustomerLoginNormal(Login login);
        ProviderRegister ProviderLogin(Login login);
        ProviderRegister ProviderLoginNormal(Login login);
    }
}
