﻿using API_ServiceToHome.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Data
{
    public interface ITransactionLogService
    {
        void LogInsert(TransactionLog md);
    }
}
