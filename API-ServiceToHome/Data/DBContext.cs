﻿using API_ServiceToHome.Models;
using API_ServiceToHome.Models.View;
using API_ServiceToHome.Models.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Data
{
    public class DBContext:DbContext
    {
        public DBContext(DbContextOptions options): base(options)
        {

        }

        public DbSet<Booking> Booking { get; set; }
        public DbSet<Category> Category { get; set; }
        public DbSet<EntityType> EntityType { get; set; }
        public DbSet<CustomerBasket> CustomerBasket { get; set; }
        public DbSet<CustomerRegister> CustomerRegister { get; set; }
        public DbSet<ProviderRegister> ProviderRegister { get; set; }
        public DbSet<RoleUser> Role { get; set; }
        public DbSet<ServiceCard> ServiceCard { get; set; }
        public DbSet<TypeRegister> TypeRegister { get; set; }
        public DbSet<WalletProvider> WalletProvider { get; set; }
        public DbSet<AttachmentFile> AttachmentFile { get; set; }
        public DbSet<MasterCancelReason> MasterCancelReason { get; set; }

        public DbSet<WorkflowProcessBooking> WorkflowProcessBooking { get; set; }
        public DbSet<ModuleVariable> ModuleVariable { get; set; }

        public DbSet<ServiceCardMapping> ServiceCardMapping { get; set; }
        public DbSet<ServiceCardReview> ServiceCardReview { get; set; }
        public DbSet<TransactionLog> TransactionLog { get; set; }
        public DbSet<ProviderTracking> ProviderTracking { get; set; }


        public DbQuery<ServiceCardModel> ViewServiceCards { get; set; }
        public DbQuery<HistoryBookingModel> ViewHistoryBooking { get; set; }
        public DbQuery<BookingModel> ViewBookings { get; set; }
        public DbQuery<WorkflowProcessBookingModel> ViewWorkflowProcessBooking { get; set; }

        

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Query<HistoryBookingModel>().ToView("View_BookingHistory");
            modelBuilder.Query<ServiceCardModel>().ToView("View_ServiceCard");
            modelBuilder.Query<BookingModel>().ToView("View_Booking");
            modelBuilder.Query<WorkflowProcessBookingModel>().ToView("View_WorkflowProcessBooking");
        }






        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
            //modelBuilder.Entity<ServiceCard>()
            //    .HasOne(p => p.ProviderRegister)
            //    .WithMany(b => b.ServiceCard)
            //    .HasForeignKey(p => p.ProviderID);

    //        modelBuilder.Entity<Booking>(entity =>
    //        {
    //            entity.HasOne(d => d.Status)
    //.WithMany(p => p.Booking)
    //.HasForeignKey(d => d.StatusId)
    //.HasConstraintName("FK_Booking_BookingStatus");
    //        });

            //modelBuilder.Entity<ServiceCard>()
            //            .HasOne(p => p.ProviderRegister)
            //            .WithMany(b => b.ServiceCard)
            //            .HasForeignKey(p => p.ProviderID);

            //modelBuilder.Entity<Card>()
            //    .HasOne(s => s) // Mark Address property optional in Student entity
            //    .WithRequired(ad => ad.Student);
        //}

    }
}
