﻿
using API_ServiceToHome.Models;
using API_ServiceToHome.Models.InputModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Data
{
    public interface IBookingAction
    {
        Booking CreateRequest(BookingInputModel md);//1
        void AssignBookingToProvider(BookingAssignJob md);
        void Accepted(int? bookId);//2
        void Pending(BookIDOrBookCode md);//3
        void InProcess(int? bookId); // 4
        void Completed(BookingActionInputModel md); //5
        void Closed(BookingActionInputModel md); //6
        void Canceled(BookingActionInputModel md);
        void Reject(BookingActionInputModel md);
     
        void AddWorkflowProcessBooking(int BookingID,int StatusID);
        void Save();
    }
}
