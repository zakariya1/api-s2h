﻿using API_ServiceToHome.Models;
using API_ServiceToHome.Models.InputModels;
using API_ServiceToHome.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace API_ServiceToHome.Data.EFCore
{
    public class History : IHistory
    {
        private readonly DBContext dBContext;

        public History(DBContext dBContext)
        {
            this.dBContext = dBContext;
        }

        public List<HistoryBookingModel> GetAllBookingHistory()
        {

            return dBContext.ViewHistoryBooking.ToList();
        }

        public List<HistoryBookingModel> GetAllBookingHistoryByCustID(HistoryFilter filters)
        {
            List<HistoryBookingModel> historyBookings = new List<HistoryBookingModel>();
            if (filters.CustomerID != null && filters.StatusID != null)
            {
                var result =  dBContext.ViewHistoryBooking
                                        .Where(x => x.CustomerID == filters.CustomerID && x.StatusID == filters.StatusID)
                                        .ToList();
                historyBookings = result;
            }
            else if (filters.CustomerID != null && filters.StatusID == null)
            {
                var result = dBContext.ViewHistoryBooking
                                        .Where(x => x.CustomerID == filters.CustomerID)
                                        .ToList();
                historyBookings = result;
            }

            return historyBookings;

            //return dBContext.ViewHistoryBooking
            //                .Where(x => x.CustomerID == CustID && x.StatusID == StatusID)
            //                .ToList();
        }

        public HistoryBookingModel GetHistoryDetailByBookingID(int BookingID)
        {
            return dBContext.ViewHistoryBooking.FirstOrDefault(o => o.BookID == BookingID);
        }

        //public void Save()
        //{
        //    throw new NotImplementedException();
        //}
    }
}
