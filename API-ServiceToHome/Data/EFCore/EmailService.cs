﻿using API_ServiceToHome.Models;
using API_ServiceToHome.Models.InputModels;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace API_ServiceToHome.Data.EFCore
{
    public class EmailService : IEmailService
    {
        private readonly DBContext dBContext;
        private IFactoryService factoryService;

        public EmailService(DBContext dBContext, IFactoryService factoryService)
        {
            this.dBContext = dBContext;
            this.factoryService = factoryService;
        }

        public void ActivateEmailCustomer(string gUID)
        {

            var result = dBContext.CustomerRegister.FirstOrDefault(x => x.UserGUID == gUID);
            if (result != null)
            {
                result.isVerify = true;
                result.ModifiedDate = DateTime.Now;

                dBContext.CustomerRegister.Update(result);
                dBContext.SaveChanges();
            }
        }

        public void ActivateEmailProvider(string gUID)
        {

            var result = dBContext.ProviderRegister.FirstOrDefault(x => x.UserGUID == gUID);
            if (result != null)
            {
                result.isVerify = true;
                result.ModifiedDate = DateTime.Now;

                dBContext.ProviderRegister.Update(result);
                dBContext.SaveChanges();
            }
            //#region Log Insert 
            //TransactionLog log = new TransactionLog();
            //log.ActivityType = "Provider Active Mail";
            //log.EntityName = "ActivateEmailProvider";
            //log.EntityTypeID = null;
            //log.ReferID = result == null ? null : result.ProviderID.ToString();
            //log.RequestData = gUID;
            //log.Detail = Newtonsoft.Json.JsonConvert.SerializeObject(result);
            //log.Result = "success";
            //logTran.LogInsert(log);
            //#endregion
        }


        public bool SendEmailActive(string UserGUID,string Email,string CustomerOrProvder)
        {

            Email mail = new Email();
            string UrlActivate = string.Empty;
            if (CustomerOrProvder == Helper.EnumHelper.CodeTypeUse.CUSTOMER)
            {
                UrlActivate = factoryService.GetAppSetting("Email", "UrlActiveCustomer") + UserGUID;
            }
            else if(CustomerOrProvder  == Helper.EnumHelper.CodeTypeUse.PROVIDER)
            {
                UrlActivate = factoryService.GetAppSetting("Email", "UrlActiveProvider") + UserGUID;
            }
            
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<h1>Thank you for signing up with us.</h1> <br><br>");
            sb.AppendLine("Please <b>verify</b> your email address by clicking the link below: <br>");
            sb.AppendLine($"<h2><font color=#336699><b><a href = '{UrlActivate}'>Verify email address</a></b></font></h2> <br><br><br><br>");
            sb.AppendLine("if you have any questions, just contact us.<br>");
            sb.AppendLine("<img src = 'https://apisvth.wherey.co/api/UploadAndPreview/20200525ORTHOR_9669a647-810f-4ec4-9638-ae91d72bed20.png' style='width: 15px; height: 15px; '>");
            sb.AppendLine("<font color=#336699><b><a href = 'https://line.me/R/ti/p/%40716aeeml'>Line</a></b></font><br>");
            sb.AppendLine("<img src = 'https://apisvth.wherey.co/api/UploadAndPreview/20200525ORTHOR_504ac020-71ae-45eb-b924-0d31254547d3.png' style='width: 15px; height: 15px; '>");
            sb.AppendLine("<font color=#336699><b><a href = 'https://line.me/R/ti/p/%40716aeeml'>Facebook</a></b></font><br>");
            sb.AppendLine("<img src = 'https://apisvth.wherey.co/api/UploadAndPreview/20200525ORTHOR_2ef3e045-093b-4b24-885b-57868f31f81f.png' style='width: 15px; height: 15px; '>");
            sb.AppendLine("<font color=#336699><b><a href = 'https://line.me/R/ti/p/%40716aeeml'>Website</a></b></font><br><br>");
            sb.AppendLine("Best regards, <br>Serve Service Team.");

            mail.Subject = "Please verify your email address.";
            mail.Body = sb.ToString();
            //m.Body = $"please your click url {UrlActivate}. thak you.";
            mail.To = Email;

            //get value from modulevariable
            string AuthenUser = factoryService.GetAppSetting("Email", "AuthenUser");
            string AuthenPassword = factoryService.GetAppSetting("Email", "AuthenPassword");
            string DisplayName = factoryService.GetAppSetting("Email", "DisplayName");
            string SMTPPort = factoryService.GetAppSetting("Email", "SMTPPort");
            string SMTPServer = factoryService.GetAppSetting("Email", "SMTPServer");


            bool flg;
            string EventStatus = String.Empty;
            try
            {
                MailMessage mm = new MailMessage();
                mm.To.Add(mail.To);
                mm.Subject = mail.Subject;
                mm.Body = mail.Body;
                mm.From = new MailAddress(AuthenUser, DisplayName);
                mm.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient(SMTPServer);
                smtp.Port = int.Parse(SMTPPort);
                smtp.UseDefaultCredentials = true;
                smtp.EnableSsl = true;
                smtp.Credentials = new System.Net.NetworkCredential(AuthenUser, AuthenPassword);
                smtp.Send(mm);
                flg = true;

            }
            catch
            {
                flg = false;

            }
            return flg;
        }
    }
}
