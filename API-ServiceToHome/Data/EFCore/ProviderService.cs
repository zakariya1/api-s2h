﻿using API_ServiceToHome.Models;
using API_ServiceToHome.Models.InputModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Data.EFCore
{
    public class ProviderService : IProviderService
    {
        private readonly DBContext _context;

        public ProviderService(DBContext context)
        {
            _context = context;
        }

 
        public ProviderTracking StartTrackingProvider(TrackingInputModel md)
        {
            ProviderTracking m = null;
            var result = _context.ProviderTracking.FirstOrDefault(x => x.BookID == md.BookID);
            if (result == null)
            {
                m = new ProviderTracking();
                m.BookID = md.BookID;
                m.StartLat = md.StartLat;
                m.StartLong = md.StartLong;
                m.Remark = md.Remark;
                m.StartTime = DateTime.Now;
                m.CreatedDate = DateTime.Now;
                _context.ProviderTracking.Add(m);
                _context.SaveChanges();
            }
            return m;
        }
        public ProviderTracking EndTrackingProvider(TrackingInputModel md)
        {
            var result = _context.ProviderTracking.FirstOrDefault(x => x.BookID == md.BookID);
            if (result != null)
            {
                result.EndLat = md.EndLat;
                result.EndLong = md.EndLong;
                result.Remark = md.Remark;
                result.EndTime = DateTime.Now;
                result.ModifiedDate = DateTime.Now;
                _context.ProviderTracking.Update(result);
                _context.SaveChanges();
            }
            return result;
        }
        public ProviderRegister Login(Login md)
        {
            throw new NotImplementedException();
        }

        public ProviderTracking GetLocationTracking(int? bookID = 0)
        {
            return _context.ProviderTracking.FirstOrDefault(x => x.BookID == bookID);
        }

        public ProviderRegister Register(ProviderRegister md)
        {
            throw new NotImplementedException();
        }
    }
}
