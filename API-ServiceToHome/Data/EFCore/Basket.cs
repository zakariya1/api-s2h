﻿using API_ServiceToHome.Models;
using API_ServiceToHome.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Data.EFCore
{
    public class Basket : IBasket
    {
        private readonly DBContext dBContext;

        public Basket(DBContext dBContext)
        {
            this.dBContext = dBContext;
        }

        public CustomerBasket CreateBasket(CustomerBasket md)
        {
            GenerateCode gen = new GenerateCode(dBContext);            
            var basket = new CustomerBasket();
            basket.BasketCode = gen.CreateCode(Helper.EnumHelper.CodeTypeName.BUCKET);
            basket.CustomerID = md.CustomerID;
            basket.Total = md.Total;
            basket.CreatedBy = 0;
            basket.CreatedDate = DateTime.Now;
            dBContext.CustomerBasket.Add(basket);
            dBContext.SaveChanges();
            return basket;

        }

        public void RemoveBasket(int id)
        {
            var objResult = dBContext.CustomerBasket.FirstOrDefault(x => x.CustomerBasketID == id);
            dBContext.CustomerBasket.Remove(objResult);
            dBContext.SaveChanges();
        }

        public void Save()
        {
            dBContext.SaveChanges();
        }

        public void UpdateBasket(CustomerBasket md)
        {
            dBContext.CustomerBasket.Update(md);
            dBContext.SaveChanges();
        }
    }
}
