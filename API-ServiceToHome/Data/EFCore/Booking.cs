﻿using API_ServiceToHome.Models;
using API_ServiceToHome.Models.InputModels;
using API_ServiceToHome.Models.ViewModels;
using API_ServiceToHome.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Data.EFCore
{
    public class Booking : IBooking
    {
        private readonly DBContext dBContext;

        public Booking(DBContext context)
        {
            this.dBContext = context;
        }

        public void CreateBooking(BookingInputModel input)
        {
            GenerateCode gen = new GenerateCode(dBContext);
            Models.Booking book = new Models.Booking();
            book.BookCode = gen.CreateCode(Helper.EnumHelper.CodeTypeName.BOOKING);
            book.BasketID = input.BasketID;
            book.ServiceID = input.ServiceID;
            book.StatusID = (int)Helper.EnumHelper.StatusBooking.CreateRequest;
            book.BookTypeID = input.BookTypeID;
            book.BookingTime = TimeSpan.Parse(input.BookingTime);
            book.BookingDate = input.BookingDate;
            book.CreatedDate = DateTime.Now;
            book.CreatedBy = 0;
            book.Lat = input.Lat;
            book.Long = input.Long;
            
            dBContext.Booking.Add(book);


        }

        public BookingModel GetBookingByBookID(int bookid)
        {
            BookingModel model = new BookingModel();
            if (bookid != 0)
            {
                model = dBContext.ViewBookings
                    .FirstOrDefault(x => x.BookID == bookid);    
                
            }
            return model;
        }

        public List<BookingModel> GetAllBookingFillter(BookingFillter fillter)
        {
            List<BookingModel> model = new List<BookingModel>();

            if (fillter.CustomerID != 0)
            {
                model = dBContext.ViewBookings
                    .Where(
                    x => x.CustomerID == fillter.CustomerID
                    && fillter.BookingStatusID.Contains(x.StatusID)
                    ).ToList();
                //fillter.StatusID.w
                // model.Add(list);
                //var model1 = model.Where(y => fillter.BookingStatusID.Any(x => x.));
            }

            return model;
        }



        public void RemoveBooking(int id)
        {
            var objBook = dBContext.Booking.FirstOrDefault(x => x.BookID == id);
            dBContext.Booking.Remove(objBook);
        }

        public void Save()
        {
            dBContext.SaveChanges();
        }

        public void UpdateBooking(Models.Booking book)
        {
            dBContext.Booking.Update(book);
        }

        public List<BookingModel> GetAllBookingProviderFillter(BookingFillter fillter)
        {
            List<BookingModel> model = new List<BookingModel>();

            if (fillter.ProviderID != 0)
            {
                model = dBContext.ViewBookings
                    .Where(
                    x => x.ProviderID == fillter.ProviderID
                    && fillter.BookingStatusID.Contains(x.StatusID)
                    ).ToList();
            }

            return model;
        }
    }
}
