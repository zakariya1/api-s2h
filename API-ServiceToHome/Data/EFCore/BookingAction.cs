﻿using API_ServiceToHome.Models;
using API_ServiceToHome.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API_ServiceToHome.Helper;
using API_ServiceToHome.Models.InputModels;
using Microsoft.EntityFrameworkCore;

namespace API_ServiceToHome.Data.EFCore
{
    public class BookingAction : IBookingAction
    {
        private readonly DBContext _context;

        public BookingAction(DBContext context)
        {
            _context = context;
        }




        public Models.Booking CreateRequest(Models.BookingInputModel md)
        {
            //var dbContextTransaction = _context.Database.BeginTransaction();
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    GenerateCode gen = new GenerateCode(_context);
                    var book = new Models.Booking();
                    book.BookCode = gen.CreateCode(EnumHelper.CodeTypeName.BOOKING);
                    book.BasketID = md.BasketID;
                    book.ServiceID = md.ServiceID;
                    book.BookingDetail = md.BookingDetail;
                    book.StatusID = (int)EnumHelper.StatusBooking.CreateRequest;
                    book.BookTypeID = md.BookTypeID;
                    book.BookingTime = TimeSpan.Parse(md.BookingTime);
                    book.BookingDate = md.BookingDate;
                    book.CreatedDate = DateTime.Now;
                    book.CreatedBy = 0;
                    book.Lat = md.Lat;
                    book.Long = md.Long;

                    _context.Booking.Add(book);
                    //_context.Database.ExecuteSqlCommand(@"SET IDENTITY_INSERT [arise2020].[Booking] ON");
                    _context.SaveChanges();
                    //_context.Database.ExecuteSqlCommand(@"SET IDENTITY_INSERT [arise2020].[Booking] OFF");
                    var result = _context.ServiceCard.FirstOrDefault(x => x.ServiceCardID == md.ServiceID);
                    if (result != null)
                    {
                        ServiceCardMapping scm = new ServiceCardMapping();
                        scm.ProviderID = result.ProviderID;
                        scm.ServiceID = md.ServiceID;
                        scm.BooKID = book.BookID;

                        _context.ServiceCardMapping.Add(scm);
                        _context.SaveChanges();
                    }

                    AddWorkflowProcessBooking(book.BookID, (int)EnumHelper.StatusBooking.CreateRequest);

                    transaction.Commit();
                    return book;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return null;
                }
            }

        }

        public void AssignBookingToProvider(BookingAssignJob md)
        {
            var result = _context.Booking.FirstOrDefault(x => x.BookCode == md.BookCode);
            if (result!=null)
            {
                var servicemapping = _context.ServiceCardMapping.FirstOrDefault(x => x.BooKID == result.BookID);
                if (servicemapping != null)
                {
                    servicemapping.ProviderID = md.ProviderID;
                    _context.ServiceCardMapping.Update(servicemapping);
                    _context.SaveChanges();
                }
            }
        
        }
        public void Accepted(int? bookId)
        {
            var r = _context.Booking.FirstOrDefault(x => x.BookID == bookId);
            if (r != null)
            {
                r.StatusID = (int)EnumHelper.StatusBooking.Accepted;
                r.ModifiedBy = 0;
                r.ModifiedDate = DateTime.Now;
                _context.Booking.Update(r);
                _context.SaveChanges();
                AddWorkflowProcessBooking(r.BookID, (int)EnumHelper.StatusBooking.Accepted);
            }
        }


        public void Pending(BookIDOrBookCode md)
        {
            var r = _context.Booking.FirstOrDefault(x => x.BookID == md.bookingID || x.BookCode == md.bookingCode);
            if (r != null)
            {
                r.StatusID = (int)EnumHelper.StatusBooking.Pending;
                r.ModifiedBy = 0;
                r.ModifiedDate = DateTime.Now;
                _context.Booking.Update(r);
                _context.SaveChanges();
                AddWorkflowProcessBooking(r.BookID, (int)EnumHelper.StatusBooking.Pending);
            }
        }

        public void InProcess(int? bookId)
        {
            var r = _context.Booking.FirstOrDefault(x => x.BookID == bookId);
            if (r != null)
            {
                r.StatusID = (int)EnumHelper.StatusBooking.In_Process;
                r.ModifiedBy = 0;
                r.ModifiedDate = DateTime.Now;
                _context.Booking.Update(r);
                _context.SaveChanges();
                AddWorkflowProcessBooking(r.BookID, (int)EnumHelper.StatusBooking.In_Process);
            }
        }

        public void Completed(BookingActionInputModel md)
        {
            var r = _context.Booking.FirstOrDefault(x => x.BookID == md.BookID);
            if (r != null)
            {
                r.StatusID = (int)EnumHelper.StatusBooking.Completed;
                r.ModifiedBy = 0;
                r.Comments = md.Comments;
                r.ModifiedDate = DateTime.Now;
                _context.Booking.Update(r);
                _context.SaveChanges();
                AddWorkflowProcessBooking(r.BookID, (int)EnumHelper.StatusBooking.Completed);


                if (md.Comments != null || md.Rating != 0)
                {
                    var result = _context.ViewBookings.FirstOrDefault(x => x.BookID == md.BookID);
                    if (result != null)
                    {
                        ServiceCardReview scr = new ServiceCardReview();
                        scr.ServiceCardID = result.ServiceID;
                        scr.CustomerID = result.CustomerID;
                        scr.UserTypeID = md.UserTypeID == (int)Helper.EnumHelper.UserType.Customer ? (int)Helper.EnumHelper.UserType.Customer : (int)Helper.EnumHelper.UserType.Provider;
                        scr.Comment = md.Comments;
                        scr.Rating = md.Rating;
                        scr.isActive = true;
                        scr.CreatedDate = DateTime.Now;
                        scr.CreatedBy = result.CustomerID;

                        _context.ServiceCardReview.Add(scr);
                        _context.SaveChanges();
                    }

                }
            }
        }
        public void AddWorkflowProcessBooking(int BookID, int StatusID)
        {
            var result = _context.WorkflowProcessBooking.FirstOrDefault(x => x.BookID == BookID && x.StatusID == StatusID);
            if (result == null)
            {
                WorkflowProcessBooking wf = new WorkflowProcessBooking();
                wf.BookID = BookID;
                wf.StatusID = StatusID;
                wf.ActionDate = DateTime.Now;
                wf.CreatedDate = DateTime.Now;
                _context.WorkflowProcessBooking.Add(wf);
                _context.SaveChanges();
            }
        }

        public void Canceled(BookingActionInputModel book)
        {
            var r = _context.Booking.FirstOrDefault(x => x.BookID == book.BookID);
            if (r != null)
            {
                r.StatusID = (int)EnumHelper.StatusBooking.Cancel;
                r.CancelReasonDetail = book.CancelDetail;
                r.CancelReasonID = book.CancelReasonID;
                r.IsCancel = true;
                r.ModifiedBy = 0;
                r.ModifiedDate = DateTime.Now;
                _context.Booking.Update(r);
                _context.SaveChanges();
                AddWorkflowProcessBooking(r.BookID, (int)EnumHelper.StatusBooking.Cancel);
            }
        }

        public void Closed(BookingActionInputModel book)
        {
            var r = _context.Booking.FirstOrDefault(x => x.BookID == book.BookID);
            if (r != null)
            {
                r.StatusID = (int)EnumHelper.StatusBooking.Closed;
                r.Comments = book.Comments;
                r.ModifiedBy = 0;
                r.ModifiedDate = DateTime.Now;
                _context.Booking.Update(r);
                _context.SaveChanges();
                AddWorkflowProcessBooking(r.BookID, (int)EnumHelper.StatusBooking.Closed);

                //
                if (book.Comments != null || book.Rating != 0)
                {
                    var result = _context.ViewBookings.FirstOrDefault(x => x.BookID == book.BookID);
                    if (result != null)
                    {
                        ServiceCardReview scr = new ServiceCardReview();
                        scr.ServiceCardID = result.ServiceID;
                        scr.CustomerID = result.CustomerID;
                        scr.UserTypeID = book.UserTypeID == (int)Helper.EnumHelper.UserType.Customer ? (int)Helper.EnumHelper.UserType.Customer : (int)Helper.EnumHelper.UserType.Provider;
                        scr.Comment = book.Comments;
                        scr.Rating = book.Rating;
                        scr.isActive = true;
                        scr.CreatedDate = DateTime.Now;
                        scr.CreatedBy = result.CustomerID;

                        _context.ServiceCardReview.Add(scr);
                        _context.SaveChanges();
                    }

                }
            }
        }
        public void Reject(BookingActionInputModel book)
        {
            var r = _context.Booking.FirstOrDefault(x => x.BookID == book.BookID);
            if (r != null)
            {
                r.StatusID = (int)EnumHelper.StatusBooking.Reject;
                r.IsCancel = true;
                r.CancelUserTypeID = 2;
                r.CancelReasonID = book.CancelReasonID;
                r.CancelReasonDetail = book.CancelDetail;
                r.CancelDate = DateTime.Now;
                r.ModifiedBy = 0;
                r.ModifiedDate = DateTime.Now;
                _context.Booking.Update(r);
                _context.SaveChanges();
                AddWorkflowProcessBooking(r.BookID, (int)EnumHelper.StatusBooking.Reject);
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }

    }
}
