﻿using API_ServiceToHome.Models;
using API_ServiceToHome.Models.InputModels;
using API_ServiceToHome.Models.View;
using API_ServiceToHome.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace API_ServiceToHome.Data.EFCore
{
    public class ServiceCard : IServiceCard
    {
        private readonly DBContext dBContext;
        private IFactoryService factoryService;
        private IOptions<AppSettingsModel> settings;
        private readonly ILogger<ServiceCard> logger;

        private static string urlImg = string.Empty;

        public ServiceCard(DBContext dBContext, IFactoryService factoryService)
        {
            this.dBContext = dBContext;
            this.factoryService = factoryService;
            urlImg = factoryService.GetAppSetting("Common", "ViewFileUrl");
        }

        public void AddServiceCared(Models.ServiceCard md)
        {
            GenerateCode gen = new GenerateCode(dBContext);
            md.ServiceCode = gen.CreateCode(Helper.EnumHelper.CodeTypeName.SERVICECARD);
            md.CreatedDate = DateTime.Now;
            md.CreatedBy = 0;
            md.ModifiedBy = null;
            md.ModifiedDate = null;
            dBContext.ServiceCard.Add(md);
        }

        public void UpdateServiceCared(int id, Models.ServiceCard md)
        {
            var r = dBContext.ServiceCard.FirstOrDefault(x => x.ServiceCardID == id);
            if (r != null)
            {
                r.ServiceName = md.ServiceName;
                r.CategoryID = md.CategoryID;
                r.Price = md.Price;
                r.PricePromote = md.PricePromote;
                r.Discount = md.Discount;
                r.Description = md.Description;
                r.isPromote = md.isPromote;
                r.ModifiedDate = DateTime.Now;
                r.ModifiedBy = 0;
            }
            dBContext.ServiceCard.Update(r);
        }

        public void RemoveServiceCared(int ServiceId)
        {
            var result = dBContext.ServiceCard.FirstOrDefault(x => x.ServiceCardID == ServiceId);
            dBContext.ServiceCard.Remove(result);
        }

        public void Save()
        {
            dBContext.SaveChanges();
        }

        public List<ServiceCardModel> GetServiceCardAll()
        {
            List<ServiceCardModel> listmodel = new List<ServiceCardModel>();
            var result = dBContext.ViewServiceCards.ToList();
            foreach (var item in result)
            {

                var imgvalue = dBContext.AttachmentFile
                    .Where(x => x.ReferID == item.ServiceCardID
                    && x.EntityTypeID == (int)Helper.EnumHelper.EntityType.SERVICECARD
                    && x.MainDisplay == true)
                    .FirstOrDefault();


                if (imgvalue != null) { item.ServiceImg = urlImg + imgvalue.FilePath; }

                listmodel.Add(item);
            }
            return listmodel;
        }

        public ServiceCardModel GetServiceCardByServiceId(int ServiceId)
        {
            string urlImg = settings.Value.PathViewfile;
            ServiceCardModel Card = dBContext.ViewServiceCards.FirstOrDefault(x => x.ServiceCardID == ServiceId && x.isActive == true);
            if (Card != null)
            { 
                //LOGIC09
                var imgvalue = dBContext.AttachmentFile
                    .Where(x => x.ReferID == Card.ServiceCardID
                    && x.EntityTypeID == (int)Helper.EnumHelper.EntityType.SERVICECARD
                    && x.MainDisplay == true)
                    .FirstOrDefault();
                if (imgvalue != null) { Card.ServiceImg = urlImg + imgvalue.FilePath; }
            }
            return Card;
        }

        public List<ServiceCardModel> GetServiceCardByProviderId(int ProviderId)
        {
            List<ServiceCardModel> servicecards = new List<ServiceCardModel>();
            var result = dBContext.ViewServiceCards.Where(x => x.ProviderID == ProviderId && x.isActive == true).ToList();
            foreach (var item in result)
            {

                var imgvalue = dBContext.AttachmentFile
                    .Where(x => x.ReferID == item.ServiceCardID
                    && x.EntityTypeID == (int)Helper.EnumHelper.EntityType.SERVICECARD
                    && x.MainDisplay == true)
                    .FirstOrDefault();
                if (imgvalue != null) { item.ServiceImg = urlImg + imgvalue.FilePath; }

                servicecards.Add(item);
            }
            return servicecards;
        }

        public List<ServiceCardModel> GetServiceCardFillter(ServiceCardFillter fillter)
        {
            List<ServiceCardModel> servicecards = new List<ServiceCardModel>();
            if (fillter.isPromote == true)
            {          
                var result = dBContext.ViewServiceCards.Where(x => x.isPromote == true && x.isActive == true).ToList();
                foreach (var item in result)
                {

                    var imgvalue = dBContext.AttachmentFile
                        .Where(x => x.ReferID == item.ServiceCardID
                        && x.EntityTypeID == (int)Helper.EnumHelper.EntityType.SERVICECARD
                        && x.MainDisplay == true)
                        .FirstOrDefault();
                    if (imgvalue != null) { item.ServiceImg = urlImg + imgvalue.FilePath; }

                    servicecards.Add(item);
                }
            }
            else if(fillter.CategoryID != null) 
            {
                var result = dBContext.ViewServiceCards.Where(x => x.CategoryID == fillter.CategoryID).ToList();
                foreach (var item in result)
                {

                    var imgvalue = dBContext.AttachmentFile
                        .Where(x => x.ReferID == item.ServiceCardID
                        && x.EntityTypeID == (int)Helper.EnumHelper.EntityType.SERVICECARD
                        && x.MainDisplay == true)
                        .FirstOrDefault();
                    if (imgvalue != null) { item.ServiceImg = urlImg + imgvalue.FilePath; }

                    servicecards.Add(item);
                }
                
            }
            return servicecards;
        }

        public List<ServiceCardModel> SearchServiceCard(FilterSearchService md)
        {
            List<ServiceCardModel> servicecards = new List<ServiceCardModel>();
            var result = dBContext.ViewServiceCards
                        .Where(x => x.ServiceName.Contains(md.Wording)
                        && x.isActive == true).ToList();
            foreach (var item in result)
            {

                var imgvalue = dBContext.AttachmentFile
                    .Where(x => x.ReferID == item.ServiceCardID
                    && x.EntityTypeID == (int)Helper.EnumHelper.EntityType.SERVICECARD
                    && x.MainDisplay == true)
                    .FirstOrDefault();
                if (imgvalue != null) { item.ServiceImg = urlImg + imgvalue.FilePath; }

                servicecards.Add(item);
            }
            return servicecards;
        }
    }
}
