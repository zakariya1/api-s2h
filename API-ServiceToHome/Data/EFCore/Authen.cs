﻿using API_ServiceToHome.Helper;
using API_ServiceToHome.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Data.EFCore
{
    public class Authen : IAuthen
    {
        private readonly DBContext _context;
        private IFactoryService factoryService;

        public Authen(DBContext context, IFactoryService factoryService)
        {
            _context = context;
            this.factoryService = factoryService;
        }

        public CustomerRegister CustomerLogin(Login login)
        {
            CustomerRegister model = new CustomerRegister();

            var result = _context.CustomerRegister.FirstOrDefault(o =>
                        o.Email.ToLower() == login.Username.ToLower()
                        && o.RegisterType == (int)Helper.EnumHelper.RegisType.Normal
                        && o.isActive == true);

            if (result != null && SecurityHelper.GetMD5(login.Password).ToLower() == result.Password.ToLower())
            {
                var img_url = _context.AttachmentFile.OrderByDescending(x => x.CreatedDate)
                                        .FirstOrDefault(o => o.ReferID == result.CustomerID
                                        && o.EntityTypeID == (int)Helper.EnumHelper.EntityType.PROFILE_CUSTOMER);

                if (img_url != null && img_url.FilePath != null)
                    result.ProfileImg = factoryService.GetAppSetting("Common", "ViewFileUrl") + img_url.FilePath;

                model = result;
            }
            return model;
        }

        public CustomerRegister CustomerLoginNormal(Login login)
        {
            CustomerRegister model = new CustomerRegister();

            var result = _context.CustomerRegister.FirstOrDefault(o => 
                        o.Email.ToLower() == login.Username.ToLower()
                        && o.RegisterType == (int)Helper.EnumHelper.RegisType.Normal
                        && o.isActive == true);

            if (result != null && SecurityHelper.GetMD5(login.Password).ToLower() == result.Password.ToLower())
            {
                model = result;
            }
            return model;
        }

        public ProviderRegister ProviderLogin(Login login)
        {
            ProviderRegister model = new ProviderRegister();

            var result = _context.ProviderRegister.FirstOrDefault(o => 
                        o.Email.ToLower() == login.Username.ToLower() 
                        && o.RegisterType == (int)Helper.EnumHelper.RegisType.Normal 
                        && o.isActive == true);

            if (result != null && SecurityHelper.GetMD5(login.Password).ToLower() == result.Password.ToLower())
            {
                var img_url = _context.AttachmentFile.OrderByDescending(x => x.CreatedDate)
                        .FirstOrDefault(o => o.ReferID == result.ProviderID
                        && o.EntityTypeID == (int)Helper.EnumHelper.EntityType.PROFILE_PROVIDER);

                if (img_url != null && img_url.FilePath != null)
                    result.profile_img = factoryService.GetAppSetting("Common", "ViewFileUrl") + img_url.FilePath;

                model = result;
            }
            return model;
        }

        public ProviderRegister ProviderLoginNormal(Login login)
        {
            ProviderRegister model = new ProviderRegister();

            var result = _context.ProviderRegister.FirstOrDefault(o =>
            o.Email.ToLower() == login.Username.ToLower()
            && o.RegisterType == (int)Helper.EnumHelper.RegisType.Normal
            && o.isActive == true);
            if (result != null && SecurityHelper.GetMD5(login.Password).ToLower() == result.Password.ToLower())
            {
                model = result;
            }
            return model;
        }
    }
}
