﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace API_ServiceToHome.Data.EFCore
{
    public class FactoryService :IFactoryService
    {
        private readonly DBContext _context;
        private Dictionary<string, object> _parameters = null;
        private Dictionary<string, string> _moduleConfigs = null;
        private Dictionary<string, string> _configs = null;

        public FactoryService(DBContext context)
        {
            _context = context;
            //_parameters = parameters;
            //_moduleConfigs = moduleConfigs;
            //_configs = configs;
        }

        public string GetAppSetting(string ConfigName)
        {
            if (_configs == null) _configs = _context.ModuleVariable.ToDictionary(o => o.ConfigName, o => o.ConfigValue);
            return _configs.ContainsKey(ConfigName) ? _configs[ConfigName] : "";
        }

        public string GetAppSetting(string Module, string ConfigName)
        {

            string key = string.Format("{0}_{1}", Module, ConfigName);

            // Initialize config first
            if (_moduleConfigs == null) _moduleConfigs = _context.ModuleVariable.ToDictionary(o => o.Module + "_" + o.ConfigName, o => o.ConfigValue);
            return _moduleConfigs.ContainsKey(key) ? _moduleConfigs[key] : "";
        }

        public string GetDescriptionModuleVariable(string ConfigName)
        {
            string result = null;
            var module = _context.ModuleVariable.FirstOrDefault(o => o.ConfigName == ConfigName);
            if (module != null) result = module.ConfigValue;
            return result;
        }
    }
}
