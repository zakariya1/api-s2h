﻿using API_ServiceToHome.Models;
using API_ServiceToHome.Models.InputModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;


namespace API_ServiceToHome.Data.EFCore
{
    public class AttachmentService : IAttachmentService
    {
        private readonly DBContext _context;
        private IFactoryService factoryService;

        public AttachmentService(DBContext context, IFactoryService factory)
        {
            _context = context;
            factoryService = factory;
        }

        public void DeleteFileFromFullPath(string FilePath)
        {
            if (File.Exists(FilePath))
            {
                File.Delete(FilePath);
            }
        }

        public void SaveChanges()
        {
            throw new NotImplementedException();
        }

        public string SaveFilePDF(FileInputSingleImage file)
        {
            throw new NotImplementedException();
        }

        public List<string> SaveServiceCardImages(FileInputMultiImage value)
        {
            string base_url = factoryService.GetAppSetting("Common", "ViewFileUrl");
            string hostDirectory = factoryService.GetAppSetting("Common", "RootDirectory");
            long LimitFileSize = Convert.ToInt64(factoryService.GetAppSetting("Image", "FileSize"));
            List<string> supportedTypes = factoryService.GetAppSetting("Image", "FileType").Split(',').ToList<string>();
          //  string link_img = string.Empty;
            string fileName = string.Empty;
            string ErrorMessage = string.Empty;
            string path = string.Empty;
            string filePath = string.Empty;
            string Identifier = string.Empty;
            string entitytype = string.Empty;
            string subfolder = string.Empty;
            string fullPath = string.Empty;

            List<string> link_img = new List<string>();


            if (value.ReferID != 0 && value.EntityTypeID != 0 && value.Files != null)
            {
                var objData = _context.EntityType.FirstOrDefault(x => x.EntityID == value.EntityTypeID);
                if (objData != null)
                {
                    entitytype = objData.EntityName;
                }
                else
                {
                    entitytype = "ORTHOR";
                }

                subfolder = DateTime.Now.ToString("yyyyMMdd") + "_" + entitytype + "_";

                path = hostDirectory + entitytype;


                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                foreach (var file in value.Files)
                {
                    Identifier = Guid.NewGuid().ToString();
                    fileName = subfolder + Identifier + "." + Path.GetFileName(file.ContentType);
                    filePath = fileName;
                    fullPath = path + @"\" + fileName;
                    using (FileStream stream = new FileStream(Path.Combine(path, fileName), FileMode.Create))
                    {

                        var fileExt = System.IO.Path.GetExtension(file.FileName).Substring(1);
                        if (!supportedTypes.Contains(fileExt))
                        {
                            link_img.Add("File Extension Is InValid - Only Upload png/jpg/jpeg File");
                            return link_img;
                        }
                        else if (file.Length > LimitFileSize)
                        {
                            link_img.Add("File size Should Be UpTo " + file.Length + "KB");
                            return link_img;
                        }
                        else
                        {

                            //check duplicate profile image 
                            //var attachfile = _context.AttachmentFile.FirstOrDefault(
                            //    x => x.ReferID == value.ReferID &&
                            //    x.EntityTypeID == objData.EntityID);

                            //if (attachfile != null)
                            //{
                            //     DeleteFileFromFullPath(attachfile.SaveFileName);
                            //    _context.AttachmentFile.Remove(attachfile);
                            //    _context.SaveChanges();
                            //}


                            file.CopyTo(stream);// Upload file to server
                            AttachmentFile atf = new AttachmentFile();
                            atf.ReferID = value.ReferID;
                            atf.EntityTypeID = value.EntityTypeID;
                            atf.Identifier = Identifier;
                            atf.FilePath = filePath;
                            atf.FileName = file.FileName;
                            atf.SaveFileName = fullPath;
                            atf.FileSize = file.Length;
                            atf.Type = file.ContentType;
                            atf.CreatedDate = DateTime.Now;
                            atf.CreatedBy = 0;
                            atf.ModifiedDate = DateTime.Now;
                            atf.ModifiedBy = 0;

                            _context.AttachmentFile.Add(atf);
                            _context.SaveChanges();
                        }
                    }

                    link_img.Add(base_url + filePath);
                }


            }
            return link_img;
        }

        public string SaveProfileImage(FileInputSingleImage value)
        {
            string base_url = factoryService.GetAppSetting("Common", "ViewFileUrl");
            string hostDirectory = factoryService.GetAppSetting("Common", "RootDirectory");
            long LimitFileSize = Convert.ToInt64(factoryService.GetAppSetting("Image", "FileSize"));
            List<string> supportedTypes = factoryService.GetAppSetting("Image", "FileType").Split(',').ToList<string>();
            string link_img = string.Empty;
            string fileName = string.Empty;
            string ErrorMessage = string.Empty;
            string path = string.Empty;
            string filePath = string.Empty;
            string Identifier = string.Empty;
            string entitytype = string.Empty;
            string subfolder = string.Empty;
            string fullPath = string.Empty;


            if (value.ReferID != 0 && value.EntityTypeID != 0 && value.Files != null)
            {
                var objData = _context.EntityType.FirstOrDefault(x => x.EntityID == value.EntityTypeID);
                if (objData != null)
                {
                    entitytype = objData.EntityName;
                }
                else
                {
                    entitytype = "ORTHOR";
                }

                subfolder = DateTime.Now.ToString("yyyyMMdd") +"_" + entitytype + "_";

                path = hostDirectory + entitytype;


                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                Identifier = Guid.NewGuid().ToString();
                fileName = subfolder + Identifier + "." + Path.GetFileName(value.Files.ContentType);
                filePath = fileName;
                fullPath = path + @"\" + fileName;
                using (FileStream stream = new FileStream(Path.Combine(path, fileName), FileMode.Create))
                {

                    var fileExt = System.IO.Path.GetExtension(value.Files.FileName).Substring(1);
                    if (!supportedTypes.Contains(fileExt))
                    {
                        ErrorMessage = "File Extension Is InValid - Only Upload png/jpg/jpeg File";
                        return ErrorMessage;
                    }
                    else if (value.Files.Length > LimitFileSize)
                    {
                        ErrorMessage = "File size Should Be UpTo " + value.Files.Length + "KB";
                        return ErrorMessage;
                    }
                    else
                    {

                        //check duplicate profile image 
                        var attachfile = _context.AttachmentFile.FirstOrDefault(
                            x => x.ReferID == value.ReferID &&
                            x.EntityTypeID == objData.EntityID);

                        if (attachfile != null)
                        {
                             DeleteFileFromFullPath(attachfile.SaveFileName);
                            _context.AttachmentFile.Remove(attachfile);
                            _context.SaveChanges();
                        }


                        value.Files.CopyTo(stream);// Upload file to server
                        AttachmentFile atf = new AttachmentFile();
                        atf.ReferID = value.ReferID;
                        atf.EntityTypeID = value.EntityTypeID;
                        atf.Identifier = Identifier;
                        atf.FilePath = filePath;
                        atf.FileName = value.Files.FileName;
                        atf.SaveFileName = fullPath;
                        atf.FileSize = value.Files.Length;
                        atf.Type = value.Files.ContentType;
                        atf.CreatedDate = DateTime.Now;
                        atf.CreatedBy = 0;
                        atf.ModifiedDate = DateTime.Now;
                        atf.ModifiedBy = 0;

                        _context.AttachmentFile.Add(atf);
                        _context.SaveChanges();
                    }

                    link_img = base_url + filePath;
                }


            }
            return link_img;
        }

        public Byte[] Viewfile(string filepath)
        {
            string rootpath = factoryService.GetAppSetting("Common","RootDirectory");
            var result = _context.AttachmentFile.FirstOrDefault(x => x.FilePath == filepath);
            Byte[] b = System.IO.File.ReadAllBytes(Path.Combine(result.SaveFileName));   // You can use your own method over here.        
            return b;
        }


    }
}
