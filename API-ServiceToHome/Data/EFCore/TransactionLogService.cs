﻿using API_ServiceToHome.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Data.EFCore
{
    public class TransactionLogService : ITransactionLogService
    {
        private readonly DBContext _context;

        public TransactionLogService(DBContext context)
        {
            _context = context;
        }

        public void LogInsert(TransactionLog md)
        {
            md.CreatedDate = DateTime.Now;
            _context.TransactionLog.Add(md);
            _context.SaveChanges();
        }
    }
}
