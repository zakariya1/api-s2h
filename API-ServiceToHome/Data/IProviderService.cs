﻿using API_ServiceToHome.Models;
using API_ServiceToHome.Models.InputModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Data
{
    public interface IProviderService
    {
        ProviderRegister Login(Login md);
        ProviderRegister Register(ProviderRegister md);
        ProviderTracking StartTrackingProvider(TrackingInputModel md);
        ProviderTracking EndTrackingProvider(TrackingInputModel md);
        ProviderTracking GetLocationTracking(int? bookID = 0);

    }
}
