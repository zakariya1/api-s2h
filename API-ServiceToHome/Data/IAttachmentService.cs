﻿using API_ServiceToHome.Models.InputModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Data
{
    public interface IAttachmentService
    {
        string SaveProfileImage(FileInputSingleImage file);
        List<string> SaveServiceCardImages(FileInputMultiImage files);
        string SaveFilePDF(FileInputSingleImage file);
        Byte[] Viewfile(string file);
        void DeleteFileFromFullPath(string FilePath);


        void SaveChanges();

    }
}
