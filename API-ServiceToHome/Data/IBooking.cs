﻿using API_ServiceToHome.Models;
using API_ServiceToHome.Models.InputModels;
using API_ServiceToHome.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Data
{
    public interface IBooking
    {
        void CreateBooking(BookingInputModel book);
        void UpdateBooking(Booking book);
        List<BookingModel> GetAllBookingFillter(BookingFillter fillter);
        List<BookingModel> GetAllBookingProviderFillter(BookingFillter fillter);
        BookingModel GetBookingByBookID(int bookid);
        void RemoveBooking(int id);
        void Save();
    }
}
