﻿using API_ServiceToHome.Models;
using API_ServiceToHome.Models.InputModels;
using API_ServiceToHome.Models.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Data
{
    public interface IServiceCard
    {
        void AddServiceCared(ServiceCard md);
        void UpdateServiceCared(int id,ServiceCard md);
        void RemoveServiceCared(int md);
        void Save();
        List<ServiceCardModel> GetServiceCardAll();
        ServiceCardModel GetServiceCardByServiceId(int ServicId);
       
        List<ServiceCardModel> GetServiceCardByProviderId(int ProviderId);
        List<ServiceCardModel> GetServiceCardFillter(ServiceCardFillter fillter);
        List<ServiceCardModel> SearchServiceCard(FilterSearchService ServiceName);

    }
}
