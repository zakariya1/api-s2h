﻿using API_ServiceToHome.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Data
{
    public interface ICustomer
    {
        List<CustomerRegister> GetAll();
        CustomerRegister GetById(int id);
        void Insert(CustomerRegister customer);
        void Update(CustomerRegister customer);
        void Delete(CustomerRegister customer);
        void Save();
    }
}
