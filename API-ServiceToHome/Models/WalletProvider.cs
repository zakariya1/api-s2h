﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Models
{
    public class WalletProvider
    {
        [Key]
        public int WalletProviderID { get; set; }
        public int ProviderID { get; set; }
        public int Banlance { get; set; }
        public int Credit { get; set; }
        public Nullable<DateTime> CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public Nullable<DateTime> ModifiedDate { get; set; }
        public int? ModifiedBy { get; set; }
    }
}
