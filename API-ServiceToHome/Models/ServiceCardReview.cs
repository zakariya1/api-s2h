﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Models
{
    public class ServiceCardReview
    {
        [Key]
        public int ReviewID { get; set; }
        public int? ServiceCardID { get; set; }
        public int? CustomerID { get; set; }
        public int? UserTypeID { get; set; }
        public string Comment { get; set; }
        public int? Rating { get; set; }
        public bool isActive { get; set; }
        public int? CreatedBy { get; set; }
        public Nullable<DateTime> CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public Nullable<DateTime> ModifiedDate { get; set; }

    }
}
