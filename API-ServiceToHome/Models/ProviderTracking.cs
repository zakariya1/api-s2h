﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Models
{
    public class ProviderTracking
    {
        [Key]
        public int? TrackingID { get; set; }
        public int? BookID { get; set; }
        public Nullable<DateTime> StartTime { get; set; }
        public Nullable<DateTime> EndTime { get; set; }
        public double? StartLat { get; set; }
        public double? StartLong { get; set; }
        public double? EndLat { get; set; }
        public double? EndLong { get; set; }
        public string Remark { get; set; }
        public int? CreatedBy { get; set; }
        public Nullable<DateTime> CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public Nullable<DateTime> ModifiedDate { get; set; }

    }
}
