﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Models
{
    public class AttachmentFile
    {
        [Key]
        public int FileID {get; set;}
        public int ReferID { get; set; }
        public int EntityTypeID { get; set; }
        public string Identifier { get; set; }
        public string FilePath { get; set; }
        public string FileName { get; set; }
        public string SaveFileName { get; set; }
        public long FileSize { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public Nullable<DateTime> CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public Nullable<DateTime> ModifiedDate { get; set; }
        public int? ModifiedBy { get; set; }

        public bool MainDisplay { get; set; }
    }
}
