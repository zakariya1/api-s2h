﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Models
{
    public class MasterCancelReason
    {
        [Key]
        public int? CancelReasonID { get; set; }
        public string CancelReasonDetail { get; set; }
        public bool? IsActive { get; set; }
        public int? UserTypeID { get; set; }
        public int? SequenceNumber { get; set; }
        public Nullable<DateTime> CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public Nullable<DateTime> ModifiedDate { get; set; }
        public int? ModifiedBy { get; set; }
    }
}
