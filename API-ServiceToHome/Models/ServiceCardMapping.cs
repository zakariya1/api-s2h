﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Models
{
    public class ServiceCardMapping
    {
        [Key]
        public int MappingID { get; set; }
        public int ServiceID { get; set; }
        public int ProviderID { get; set; }
        public int BooKID { get; set; }
    }
}
