﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Models
{
    public class AppSettingsModel
    {
        public string HostPathDirectory { get; set; }
        public string PathViewfile { get; set; }
        public string PathSaveFile { get; set; }
        public string EmailAddress { get; set; }
        public string EmailPassword { get; set; }
        public string EmailDisplayName { get; set; }      
        public string BaseUrl { get; set; }
        public string BaseUrlMailActive { get; set; }


    }
}
