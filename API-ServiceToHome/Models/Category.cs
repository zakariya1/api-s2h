﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Models
{
    public class Category
    {
        [Key]
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public string IconDisplay { get; set; }
        public bool IsReqCer { get; set; }
        public Nullable<DateTime> Create_date { get; set; }
        public int? Create_by { get; set; }
        public Nullable<DateTime> Update_date { get; set; }
        public int? Update_by { get; set; }
    }
}
