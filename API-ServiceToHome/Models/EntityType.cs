﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Models
{
    public class EntityType
    {
         [Key]
        public int EntityID { get; set; }
        public string EntityName { get; set; }
    }
}
