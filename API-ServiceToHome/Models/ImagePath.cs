﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Models
{
    public class ImagePath
    {
        public string FilePath { get; set; }
    }
}
