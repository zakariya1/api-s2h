﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Models
{

    public class SetLocation
    {
        public enum EntityType { CUSTOMER = 1, PROVIDER = 2,  BOOKING = 3 }
        public int ReferID { get; set; }
        public EntityType Type  { get; set; }
        public double? Lat { get; set; }
        public double? Long { get; set; }

    }

}



