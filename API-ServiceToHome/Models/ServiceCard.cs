﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Models
{
    public class ServiceCard
    {
        [Key]
        public int ServiceCardID { get; set; }
        public string ServiceCode { get; set; }
        public string ServiceName { get; set; }
        public int ProviderID { get; set; }
        public int CategoryID { get; set; }
        public double? Price { get; set; }
        public double? PricePromote { get; set; }
        public double? Discount { get; set; }
        public string Description { get; set; }
        public string ServiceImg { get; set; }
        public bool isPromote { get; set; }
        public bool isActive { get; set; }
        public Nullable<DateTime> CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public Nullable<DateTime> ModifiedDate { get; set; }
        public int? ModifiedBy { get; set; }
        //public ProviderRegister ProviderRegister { get; set; }
    }
}
