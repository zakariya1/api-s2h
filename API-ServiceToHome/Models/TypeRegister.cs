﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Models
{
    public class TypeRegister
    {
        [Key]
        public int TypeRegisterID { get; set; }
        public string Type { get; set; }
        public Nullable<DateTime> Create_date { get; set; }
        public int? Create_by { get; set; }
        public Nullable<DateTime> Update_date { get; set; }
        public int? Update_by { get; set; }
    }
}
