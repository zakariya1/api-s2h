﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Models
{
    public class Booking
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BookID { get; set; }
        public string BookCode { get; set; }
        public int BasketID { get; set; }
        public int ServiceID  { get; set; }
        public int StatusID { get; set; }
        public int BookTypeID { get; set; }
        public Nullable<TimeSpan> StartTime { get; set; }
        public Nullable<TimeSpan> EndTime { get; set; }
        public Nullable<DateTime> BookingDate { get; set; }
        public Nullable<TimeSpan> BookingTime { get; set; }
        public string BookingDetail { get; set; }
        public double? Lat { get; set; }
        public double? Long { get; set; }
        public bool? IsCancel { get; set; }
        public int? CancelReasonID { get; set; }
        public string CancelReasonDetail{ get; set; }
        public string CancelBy { get; set; }
        public int? CancelUserTypeID { get; set; }
        public string Comments { get; set; }
        public Nullable<DateTime> CancelDate { get; set; }
        public Nullable<DateTime> CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public Nullable<DateTime> ModifiedDate { get; set; }
        public int? ModifiedBy { get; set; }


    }
}
