﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Models.InputModels
{
    public class FilterSearchService
    {
        public string Wording { get; set; }
    }
}
