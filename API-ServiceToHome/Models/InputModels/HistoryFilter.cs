﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Models.InputModels
{
    public class HistoryFilter
    {
        public int CustomerID { get; set; }
        public int? StatusID { get; set; }
    }
}
