﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Models.InputModels
{
    public class TrackingInputModel
    {
        public int? BookID { get; set; }
        public double? StartLat { get; set; }
        public double? StartLong { get; set; }
        public double? EndLat { get; set; }
        public double? EndLong { get; set; }
        public string Remark { get; set; }
    }
}
