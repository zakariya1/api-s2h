﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Models.InputModels
{
    public class BookingAssignJob
    {
        public string BookCode { get; set; } = "";
        public int ProviderID { get; set; } = 0;
    }
}
