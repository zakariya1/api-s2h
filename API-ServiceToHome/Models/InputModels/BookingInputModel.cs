﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Models
{
    public class BookingInputModel
    {
        
        public int BasketID { get; set; }
        public int ServiceID { get; set; }
        public int BookTypeID { get; set; }
        public string BookingDetail { get; set; }
        public string?StartTime { get; set; }
        public string? EndTime { get; set; }
        public string? BookingTime { get; set; }
        public Nullable<DateTime> BookingDate { get; set; }
        public double? Lat { get; set; }
        public double? Long { get; set; }

    }
}
