﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Models.InputModels
{
    public class BookIDOrBookCode
    {
        public int? bookingID { get; set; } = 0;
        public string bookingCode { get; set; } = "";
    }
}
