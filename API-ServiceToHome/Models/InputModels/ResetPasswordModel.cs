﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Models.InputModels
{
    public class ResetPasswordModel
    {
        public string Email { get; set; } = "";
        public string NewPassword { get; set; } = "";
        public int? UserType { get; set; }
    }
}
