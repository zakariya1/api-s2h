﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace API_ServiceToHome.Models.InputModels
{
    public class BookingActionInputModel
    {
        public int BookID { get; set; }
        public int? CancelReasonID { get; set; }
        public string CancelDetail { get; set; }
        public string RejectDetail { get; set; }
        public int? UserTypeID { get; set; }
        public string Comments { get; set; }
        public int? Rating { get; set; } = 0;
        public IFormFile? ImageReview { get; set; }
    }
}
