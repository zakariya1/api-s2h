﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Models.InputModels
{
    public class BookingFillter
    {
        public int CustomerID { get; set; }
        public int? ProviderID { get; set; }
        public List<int?> BookingStatusID { get; set; }
    }
}
