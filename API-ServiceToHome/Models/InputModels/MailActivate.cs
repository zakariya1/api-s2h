﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Models.InputModels
{
    public class MailActivate
    {
        public string MailTo { get; set; }
        public string UserGuid { get; set; }
    }
}
