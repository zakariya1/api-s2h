﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Models.InputModels
{
    public class FileInputMultiImage
    {
        public List<IFormFile> Files { get; set; } = null;
        public int ReferID { get; set; } = 0;
        public int EntityTypeID { get; set; } = 0;
    }
}
