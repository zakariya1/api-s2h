﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Models
{
    public class CustomerBasket
    {
        [Key]
        public int CustomerBasketID { get; set; }


        [Column(TypeName = "varchar(50)")]
        public string BasketCode { get; set; }
        public int CustomerID { get; set; }
        public double Total { get; set; }

        public Nullable<DateTime> CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public Nullable<DateTime> ModifiedDate { get; set; }
        public int? ModifiedBy { get; set; }
    }
}
