﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Models
{
    public class TRBooking
    {
        [Key]
        public int TRID { get; set; }
        public int BookingID { get; set; }
        [Column(TypeName = "varchar(100)")]
        public string Description { get; set; }
        public Nullable<DateTime> CreatedDate  {  get; set; }
        public Nullable<DateTime> ModifiedDate {  get; set; }
    }
}
