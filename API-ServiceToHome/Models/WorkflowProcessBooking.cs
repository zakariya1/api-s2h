﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Models
{
    public class WorkflowProcessBooking
    {
        [Key]
        public int WorkflowBookingID { get; set; }
        public int? BookID { get; set; }
        public int? StatusID { get; set; }
        public Nullable<DateTime> ActionDate { get; set; }
        public string ActionBy { get; set; }
        public Nullable<DateTime> CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<DateTime> ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
    }
}
