﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Models
{
    public class TransactionLog
    {
        [Key]
        public int LogID { get; set; }
        public string ActivityType { get; set; }
        public string EntityName { get; set; }
        public int? EntityTypeID { get; set; }
        public string ReferID { get; set; }
        public string RequestData { get; set; }
        public string Detail { get; set; }
        public string Result { get; set; }
        public string ResultDetail { get; set; }
        public int? CreatedBy { get; set; }
        public Nullable<DateTime> CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public Nullable<DateTime> ModifiedDate { get; set; }

    }
}
