﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Models.View
{
    public class ServiceCardModel
    {
        [Key]
        public int ServiceCardID { get; set; }
        public string ServiceCode { get; set; }
        public string ServiceName { get; set; }
        public string Description { get; set; }
        public string ServiceImg { get; set; }
        public double? Price { get; set; }
        public double? PricePromote { get; set; }
        public double? Discount { get; set; }
        public bool isPromote { get; set; }
        public bool isActive { get; set; }
        public int ProviderID { get; set; }
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
        public string Rating { get; set; }
        public string ProviderName { get; set; }
        public string FullName { get; set; }
        public double? Lat { get; set; }
        public double? Long { get; set; }
        public Nullable<DateTime> CreatedDate { get; set; }
    }
}
