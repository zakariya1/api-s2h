﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Models.ViewModels
{
    public class WorkflowProcessBookingModel
    {
        public int? BookID { get; set; }
        public int? StatusID { get; set; }
        public string StatusName { get; set; }
        public string StatusDisplay { get; set; }
        public string ActionDate { get; set; }
        public string ActionBy { get; set; }
        public Nullable<DateTime> CreatedDate { get; set; }
        public string CreatedBy { get; set; }
    }
}
