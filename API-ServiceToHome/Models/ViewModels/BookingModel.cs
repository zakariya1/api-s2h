﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Models.ViewModels
{
    public class BookingModel
    {
        [Key]
        public int? BookID { get; set; }
        public string BookCode { get; set; }
        public int? ProviderID { get; set; }
        public int? BasketID { get; set; }
        public string BasketCode { get; set; }
        public int? BookType { get; set; }
        public string BookingDetail { get; set; }
        public int? CustomerID { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerFullName { get; set; }
        public string CustomerName{ get; set; }
        public string Tel { get; set; }
        public string BookingDate { get; set; }
        public Nullable<TimeSpan> BookingTime { get; set; }
        public int? ServiceID { get; set; }
        public string ServiceCode { get; set; }
        public string ServiceName { get; set; }
        public double? Price { get; set; }
        public double? Total { get; set; }
        public int? StatusID { get; set; }
        public string StatusName { get; set; }
        public string StatusDisplay { get; set; }
        public double? Lat { get; set; }
        public double? Long { get; set; }

    }
}
