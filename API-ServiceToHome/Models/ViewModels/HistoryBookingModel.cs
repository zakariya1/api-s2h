﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome.Models.ViewModels
{
    public class HistoryBookingModel
    {
        public int? BookID { get; set; }
        public int? BasketID { get; set; }
        public string BasketCode { get; set; }
        public int? CustomerID { get; set; }
        public string CustomerCode { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public string Lastname { get; set; }
        public string Tel { get; set; }
        public Nullable<DateTime> BookingDate { get; set; }
        public Nullable<TimeSpan> BookingTime { get; set; }
        public int? ServiceID { get; set; }
        public string ServiceCode { get; set; }
        public string ServiceName { get; set; }
        public double? Price { get; set; }
        public double? Total { get; set; }
        public int? StatusID { get; set; }
    }
}
