﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace API_ServiceToHome.Models.View
{
    public class ProviderModel
    {
        [Key]
        public int ProviderID { get; set; }
        public string ProviderCode { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public string Lastname { get; set; }
        public string Address { get; set; }
        public string Location { get; set; }
        public string Tel { get; set; }
        public string line_id { get; set; }
        public string IDCard_num { get; set; }
        public string IDCard_Pic1 { get; set; }
        public string IDCard_Pic2 { get; set; }
        public string House_registration_PIC1 { get; set; }
        public string House_registration_PIC2 { get; set; }
        public string profile_img { get; set; }
        public int? register_type_id { get; set; }
        public int? role_id { get; set; }
        public bool? isActive { get; set; }
        public int? Token_id { get; set; }
        public string UserGUID { get; set; }
        public Nullable<DateTime> CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public Nullable<DateTime> ModifiedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public decimal? Lat { get; set; }
        public decimal? Long { get; set; }

        public virtual List<ServiceCardModel> Card { get; set; }
    }
}
