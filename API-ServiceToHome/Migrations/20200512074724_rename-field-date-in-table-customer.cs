﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace API_ServiceToHome.Migrations
{
    public partial class renamefielddateintablecustomer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Create_by",
                table: "CustomerRegister");

            migrationBuilder.DropColumn(
                name: "Create_date",
                table: "CustomerRegister");

            migrationBuilder.DropColumn(
                name: "Update_by",
                table: "CustomerRegister");

            migrationBuilder.DropColumn(
                name: "Update_date",
                table: "CustomerRegister");

            migrationBuilder.AddColumn<int>(
                name: "CreatedBy",
                table: "CustomerRegister",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedDate",
                table: "CustomerRegister",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ModifiedBy",
                table: "CustomerRegister",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedDate",
                table: "CustomerRegister",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "CustomerRegister");

            migrationBuilder.DropColumn(
                name: "CreatedDate",
                table: "CustomerRegister");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "CustomerRegister");

            migrationBuilder.DropColumn(
                name: "ModifiedDate",
                table: "CustomerRegister");

            migrationBuilder.AddColumn<int>(
                name: "Create_by",
                table: "CustomerRegister",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Create_date",
                table: "CustomerRegister",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Update_by",
                table: "CustomerRegister",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Update_date",
                table: "CustomerRegister",
                type: "datetime2",
                nullable: true);
        }
    }
}
