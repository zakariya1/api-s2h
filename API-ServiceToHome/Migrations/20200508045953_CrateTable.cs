﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace API_ServiceToHome.Migrations
{
    public partial class CrateTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AttachmentFile",
                columns: table => new
                {
                    FileID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ServiceID = table.Column<int>(nullable: false),
                    Identifier = table.Column<string>(nullable: true),
                    FilePath = table.Column<string>(nullable: true),
                    FileName = table.Column<string>(nullable: true),
                    SaveFileName = table.Column<string>(nullable: true),
                    FileSize = table.Column<int>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<int>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    ModifiedBy = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AttachmentFile", x => x.FileID);
                });

            migrationBuilder.CreateTable(
                name: "Booking",
                columns: table => new
                {
                    BookID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BasketID = table.Column<int>(nullable: false),
                    ServiceID = table.Column<int>(nullable: false),
                    BookingDate = table.Column<DateTime>(nullable: true),
                    BookingTime = table.Column<DateTime>(nullable: true),
                    Create_date = table.Column<DateTime>(nullable: true),
                    Create_by = table.Column<int>(nullable: true),
                    Update_date = table.Column<DateTime>(nullable: true),
                    Update_by = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Booking", x => x.BookID);
                });

            migrationBuilder.CreateTable(
                name: "Category",
                columns: table => new
                {
                    CategoryID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CategoryName = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    IsReqCer = table.Column<bool>(nullable: false),
                    Create_date = table.Column<DateTime>(nullable: true),
                    Create_by = table.Column<int>(nullable: true),
                    Update_date = table.Column<DateTime>(nullable: true),
                    Update_by = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Category", x => x.CategoryID);
                });

            migrationBuilder.CreateTable(
                name: "CustomerBasket",
                columns: table => new
                {
                    CustomerBasketID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CustomerID = table.Column<string>(nullable: true),
                    Total = table.Column<double>(nullable: false),
                    Create_date = table.Column<DateTime>(nullable: true),
                    Create_by = table.Column<int>(nullable: true),
                    Update_date = table.Column<DateTime>(nullable: true),
                    Update_by = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerBasket", x => x.CustomerBasketID);
                });

            migrationBuilder.CreateTable(
                name: "CustomerRegister",
                columns: table => new
                {
                    CustomerID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CustomerCode = table.Column<string>(nullable: true),
                    Username = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Lastname = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Location = table.Column<string>(nullable: true),
                    Tel = table.Column<string>(nullable: true),
                    ProfileImg = table.Column<string>(nullable: true),
                    RegisterType = table.Column<int>(nullable: true),
                    isActive = table.Column<bool>(nullable: false),
                    TokenId = table.Column<string>(nullable: true),
                    UserGUID = table.Column<string>(nullable: true),
                    Create_date = table.Column<DateTime>(nullable: true),
                    Create_by = table.Column<int>(nullable: true),
                    Update_date = table.Column<DateTime>(nullable: true),
                    Update_by = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerRegister", x => x.CustomerID);
                });

            migrationBuilder.CreateTable(
                name: "ProviderRegister",
                columns: table => new
                {
                    ProviderID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProviderCode = table.Column<string>(nullable: true),
                    Username = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Lastname = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Location = table.Column<string>(nullable: true),
                    Tel = table.Column<string>(nullable: true),
                    line_id = table.Column<string>(nullable: true),
                    IDCard_num = table.Column<string>(nullable: true),
                    IDCard_Pic1 = table.Column<string>(nullable: true),
                    IDCard_Pic2 = table.Column<string>(nullable: true),
                    House_registration_PIC1 = table.Column<string>(nullable: true),
                    House_registration_PIC2 = table.Column<string>(nullable: true),
                    profile_img = table.Column<string>(nullable: true),
                    register_type_id = table.Column<int>(nullable: true),
                    role_id = table.Column<int>(nullable: true),
                    isActive = table.Column<bool>(nullable: true),
                    Token_id = table.Column<int>(nullable: true),
                    UserGUID = table.Column<string>(nullable: true),
                    Create_date = table.Column<DateTime>(nullable: true),
                    Create_by = table.Column<int>(nullable: true),
                    Update_date = table.Column<DateTime>(nullable: true),
                    Update_by = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProviderRegister", x => x.ProviderID);
                });

            migrationBuilder.CreateTable(
                name: "Role",
                columns: table => new
                {
                    RoleID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Role = table.Column<string>(nullable: true),
                    Create_date = table.Column<DateTime>(nullable: true),
                    Create_by = table.Column<int>(nullable: true),
                    Update_date = table.Column<DateTime>(nullable: true),
                    Update_by = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Role", x => x.RoleID);
                });

            migrationBuilder.CreateTable(
                name: "ServiceCard",
                columns: table => new
                {
                    ServiceCardID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ServiceCode = table.Column<string>(nullable: true),
                    ServiceName = table.Column<string>(nullable: true),
                    ProviderID = table.Column<int>(nullable: false),
                    CategoryID = table.Column<int>(nullable: false),
                    Price = table.Column<double>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    ServiceImg = table.Column<string>(nullable: true),
                    isPromote = table.Column<bool>(nullable: false),
                    isActive = table.Column<bool>(nullable: false),
                    Create_date = table.Column<DateTime>(nullable: true),
                    Create_by = table.Column<int>(nullable: true),
                    Update_date = table.Column<DateTime>(nullable: true),
                    Update_by = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceCard", x => x.ServiceCardID);
                });

            migrationBuilder.CreateTable(
                name: "TypeRegister",
                columns: table => new
                {
                    TypeRegisterID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Type = table.Column<string>(nullable: true),
                    Create_date = table.Column<DateTime>(nullable: true),
                    Create_by = table.Column<int>(nullable: true),
                    Update_date = table.Column<DateTime>(nullable: true),
                    Update_by = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TypeRegister", x => x.TypeRegisterID);
                });

            migrationBuilder.CreateTable(
                name: "WalletProvider",
                columns: table => new
                {
                    WalletProviderID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProviderID = table.Column<int>(nullable: false),
                    Banlance = table.Column<int>(nullable: false),
                    Credit = table.Column<int>(nullable: false),
                    Create_date = table.Column<DateTime>(nullable: true),
                    Create_by = table.Column<int>(nullable: true),
                    Update_date = table.Column<DateTime>(nullable: true),
                    Update_by = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WalletProvider", x => x.WalletProviderID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AttachmentFile");

            migrationBuilder.DropTable(
                name: "Booking");

            migrationBuilder.DropTable(
                name: "Category");

            migrationBuilder.DropTable(
                name: "CustomerBasket");

            migrationBuilder.DropTable(
                name: "CustomerRegister");

            migrationBuilder.DropTable(
                name: "ProviderRegister");

            migrationBuilder.DropTable(
                name: "Role");

            migrationBuilder.DropTable(
                name: "ServiceCard");

            migrationBuilder.DropTable(
                name: "TypeRegister");

            migrationBuilder.DropTable(
                name: "WalletProvider");
        }
    }
}
