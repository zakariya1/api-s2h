﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_ServiceToHome
{
    public class AppConstant
    {
        public enum StatusBooking 
        { 
            Cancel =    -2,
            Reject =    -1,
            Request =   1,
            Paid =      2,
            Accept =    3,
            Closed =    4
        }

    }
}
